import isEmpty from "lodash/isEmpty";
import { wording } from "Wording";

export const validationName = (name) => {
  if (isEmpty(name)) {
    return wording.errorEmpty;
  }
  return null;
};

export const validationEmail = (email) => {
  if (isEmpty(email)) {
    return wording.errorEmpty;
  }
  const testEmail = /^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/.test(email);
  if (!testEmail) {
    return wording.errorEmail;
  }
  return null;
};
