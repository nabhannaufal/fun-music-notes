import { useEffect, useContext } from "react";
import { UNSAFE_NavigationContext, useNavigate } from "react-router-dom";
import { setPromptExit, setLoading } from "Containers/App/actions";
import { useDispatch } from "react-redux";

export const useBackListener = () => {
  const navigator = useContext(UNSAFE_NavigationContext).navigator;
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    const listener = (e) => {
      if (e.action === "POP") {
        dispatch(setLoading(true));
        navigate(+1);
        setTimeout(() => {
          dispatch(setLoading(false));
          dispatch(setPromptExit(true));
        }, 100);
      }
    };

    const unlisten = navigator.listen(listener);
    return unlisten;
  }, [navigator, dispatch, navigate]);
};
