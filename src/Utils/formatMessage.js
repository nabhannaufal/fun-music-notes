import { wording } from "Wording";

export const formatMessage = (key, value) => {
  if (value) {
    let text = wording[key];
    for (const keyword in value) {
      text = text.replace(`{${keyword}}`, value[keyword]);
    }

    return text;
  }
  return wording[key];
};
