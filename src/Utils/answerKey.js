import { LazyLoadImage } from "react-lazy-load-image-component";

const answerKey = (type) => {
  if (type === "level1") {
    return (
      <ol>
        <li>
          <strong>Jawaban benar: c</strong>
          <br />
          Tanda birama adalah tanda dalam notasi balok yang terletak pada awal suatu bagian dalam musik. Tanda ini
          berfungsi untuk menunjukkan satuan ketukan dan jumlah ketukan dalam birama.
        </li>
        <li>
          <strong>Jawaban benar: b</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_2.png" alt="images" />
          <br />
          Tanda birama dalam pola ritme tersebut adalah 3/4, karena dalam satu birama terdapat 3 ketukan dengan harga
          setiap not bernilai not 1/4.
        </li>
        <li>
          <strong>Jawaban benar: b</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_3.png" alt="images" />
          <br />
          Tanda birama dalam pola ritme tersebut adalah 4/4, karena dalam satu birama terdapat 4 ketukan dengan harga
          setiap not bernilai not 1/4.
        </li>
        <li>
          <strong>Jawaban benar: a</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_4.png" alt="images" />
          <br />
          Tanda birama dalam pola ritme tersebut adalah 3/2, karena dalam satu birama terdapat 3 ketukan dengan harga
          setiap not bernilai not 1/2.
        </li>
        <li>
          <strong>Jawaban benar: c</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_5.png" alt="images" />
          <br />
          Tanda birama dalam pola ritme tersebut adalah 6/8, karena dalam satu birama terdapat 6 ketukan dengan harga
          setiap not bernilai not 1/8.
        </li>
        <li>
          <strong>Jawaban benar: d</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_6.png" alt="images" />
          <br />
          Tanda birama dalam pola ritme tersebut adalah 12/8, karena dalam satu birama terdapat 12 ketukan dengan harga
          setiap not bernilai not 1/8.
        </li>
        <li>
          <strong>Jawaban benar: d</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_7.png" alt="images" />
          <br />
          Tanda birama dalam pola ritme tersebut adalah 9/8, karena dalam satu birama terdapat 9 ketukan dengan harga
          setiap not bernilai not 1/8.
        </li>
        <li>
          <strong>Jawaban benar: a</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_8.png" alt="images" />
          <br />
          Tanda kunci dalam gambar tersebut adalah kunci G.
        </li>
        <li>
          <strong>Jawaban benar: c</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_9.png" alt="images" />
          <br />
          Tanda kunci dalam gambar tersebut adalah kunci F.
        </li>
        <li>
          <strong>Jawaban benar: b</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level1_10.png" alt="images" />
          <br />
          Tanda kunci dalam gambar tersebut adalah kunci C.
        </li>
      </ol>
    );
  }
  if (type === "level2") {
    return (
      <ol>
        <li>
          <strong>Jawaban benar: b. 2 ketuk</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_1.png" alt="images" />
          <br />
          tanda berikut merupakan not setengah (1/2) yang memiliki nilai 2 ketuk, artinya dalam setiap nada yang
          dituliskan dengan tanda tersebut dimainkan selama 2 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: d. 4 ketuk</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_2.png" alt="images" />
          <br />
          tanda berikut merupakan not utuh (penuh) yang memiliki nilai 4 ketuk, artinya dalam setiap nada yang
          dituliskan dengan tanda tersebut dimainkan selama 4 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: a. 1 ketuk</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_3.png" alt="images" />
          <br />
          tanda berikut merupakan not seperempat (1/4) yang memiliki nilai 1 ketuk, artinya dalam setiap nada yang
          dituliskan dengan tanda tersebut dimainkan selama 1 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: b. 1/2 ketuk</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_4.png" alt="images" />
          <br />
          tanda berikut merupakan not seperdelapan (1/8) yang memiliki nilai 1/2 ketuk, artinya dalam setiap nada yang
          dituliskan dengan tanda tersebut dimainkan selama 1/2 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: c. </strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_5c.png" alt="images" />
          <br />
          Not tersebut merupakan not seperempat (1/4) dan bernilai 1 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: a. </strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_6a.png" alt="images" />
          <br />
          Not tersebut merupakan not seperenambelas (1/16) dan bernilai 1/4 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: c. </strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_7c.png" alt="images" />
          <br />
          Tanda tersebut merupakan tanda istirahat dalam not seperempat (1/4) yang bernilai istirahat 1 ketukan.
        </li>
        <li>
          <strong>Jawaban benar: a. G </strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_8.png" alt="images" />
          <br />
          Nada tersebut memiliki nama nada G dan dituliskan dengan menggunakan kunci G.
        </li>
        <li>
          <strong>Jawaban benar: c. C </strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_9.png" alt="images" />
          <br />
          Nada tersebut memiliki nama nada C dan dituliskan dengan menggunakan kunci C.
        </li>
        <li>
          <strong>Jawaban benar: c. F </strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level2_10.png" alt="images" />
          <br />
          Nada tersebut memiliki nama nada F dan dituliskan dengan menggunakan kunci F.
        </li>
      </ol>
    );
  }
  if (type === "level3") {
    return (
      <ol>
        <li>
          <strong>Jawaban benar: b. A minor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_1.png" alt="images" />
          <br />
          Tangga nada A minor merupakan tangga nada yang seluruh nadanya belum mengalami perubahan dan dinamakan tangga
          nada minor natural. Susunan nada dalam tangga nada a minor adalah a - b - c - d - e - f - g - a.
        </li>
        <li>
          <strong>Jawaban benar: a. C Mayor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_2.png" alt="images" />
          <br />
          Tangga nada A minor merupakan tangga nada yang seluruh nadanya belum mengalami perubahan dan dinamakan tangga
          nada minor natural. Susunan nada dalam tangga nada a minor adalah a - b - c - d - e - f - g - a.
        </li>
        <li>
          <strong>Jawaban benar: c. D Mayor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_3.png" alt="images" />
          <br />
          Dalam tangga nada D mayor, susunan nada yang mendapat perubahan atau dinaikkan 1 semitone (setengah) adalah
          nada ke-3 (tiga) dan nada ke-7 (tujuh). Sehingga tangga nada D mayor tersusun menjadi D - E - Fis - G - A - B
          - Cis - D.
        </li>
        <li>
          <strong>Jawaban benar: b. G Mayor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_4.png" alt="images" />
          <br />
          Dalam tangga nada G mayor, susunan nada ke-7 (tujuh) mengalami perubahan, yaitu dinaikkan 1 semitone
          (setengah), karena dalam nada ke 7-1 harus memiliki jarak 1/2 (setengah) atau 1 semitone. Sehingga tangga nada
          G mayor tersusun menjadi G - A - B - C - D - E - Fis - G.
        </li>
        <li>
          <strong>Jawaban benar: d. F Mayor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_5.png" alt="images" />
          <br />
          Dalam tangga nada F mayor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) nada
          adalah nada ke-4 (empat). Sehingga tangga nada F mayor tersusun menjadi F - G - A - Bes - C - D - E - F.
        </li>
        <li>
          <strong>Jawaban benar: a. E minor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_6.png" alt="images" />
          <br />
          Dalam tangga nada e minor, susunan nada ke-2 mengalami perubahan, yaitu dinaikkan 1 semitone (setengah),
          karena dalam nada ke 2-3 harus memiliki jarak 1/2 (setengah) atau 1 semitone. Sehingga tangga nada e minor
          tersusun menjadi e - fis - g - a - b - c - d - e.
        </li>
        <li>
          <strong>Jawaban benar: b. B minor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_7.png" alt="images" />
          <br />
          Dalam tangga nada b minor, susunan nada yang mendapat perubahan atau dinaikkan 1 semitone (setengah) adalah
          nada ke-2 (dua) dan nada ke-5 (lima). Sehingga tangga nada b minor tersusun menjadi b - cis - d - e - fis - g
          - a - b.
        </li>
        <li>
          <strong>Jawaban benar: c. Bes Mayor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_8.png" alt="images" />
          <br />
          Dalam tangga nada Bes mayor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) nada
          adalah nada ke-1 (satu) dan nada ke-4 (empat). Sehingga tangga nada Bes mayor tersusun menjadi Bes - C - D -
          Es - F - G - A - Bes.
        </li>
        <li>
          <strong>Jawaban benar: a. G minor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_9.png" alt="images" />
          <br />
          Dalam tangga nada g minor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) adalah
          nada ke-3 (tiga) dan nada ke -6 (enam). Sehingga tangga nada g minor tersusun menjadi g - a - bes - c - d - es
          - f - g.
        </li>
        <li>
          <strong>Jawaban benar: a. Es Mayor</strong>
          <br />
          <LazyLoadImage effect="blur" src="/soal/level3_10.png" alt="images" />
          <br />
          Dalam tangga nada Es mayor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) nada
          adalah nada ke-1 (satu), nada ke-4 (empat), dan nada ke-5 (lima). Sehingga tangga nada Es mayor tersusun
          menjadi Es - F - G - As - Bes - C - D - Es.
        </li>
      </ol>
    );
  }
};

export default answerKey;
