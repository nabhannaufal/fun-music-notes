import axios from "axios";

const urls = {
  sheetdb1: "https://sheetdb.io/api/v1/hnfn2um3e0k9z?sheet=Level1",
  sheetdb2: "https://sheetdb.io/api/v1/hnfn2um3e0k9z?sheet=Level2",
  sheetdb3: "https://sheetdb.io/api/v1/hnfn2um3e0k9z?sheet=Level3",
  sheetdb4: "https://sheetdb.io/api/v1/hnfn2um3e0k9z?sheet=Level4",
};

export const callAPI = (endpoint, method, headers = {}, params = {}, data = {}) => {
  const options = {
    url: endpoint,
    method,
    headers,
    data,
    params,
  };

  return axios(options).then((response) => {
    const responseAPI = response.data;
    return responseAPI;
  });
};

export const postSheet = (payload, type) => {
  let url = urls.sheetdb1;
  if (type === "level2") {
    url = urls.sheetdb2;
  }
  if (type === "level3") {
    url = urls.sheetdb3;
  }
  if (type === "level4") {
    url = urls.sheetdb4;
  }
  return callAPI(url, "post", {}, {}, payload);
};
