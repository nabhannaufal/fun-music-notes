import React, { useEffect } from "react";

import { wording } from "Wording";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import profile from "Images/profile.png";

const Profile = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);
  return (
    <div className={classes.content}>
      <Decoration type="dtop6" />
      <Decoration type="dbot9" />
      <TopNavigation />
      <div className={classes.menuWrapper}>
        <div className={classes.materi}>
          <div className={classes.materiTitle} dangerouslySetInnerHTML={{ __html: wording.profile }} />
          <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.profileText }} />
        </div>
        <img alt={wording.images} className={classes.profile} src={profile} />
      </div>
    </div>
  );
};

export default Profile;
