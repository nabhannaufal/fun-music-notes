import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import { wording } from "Wording";

import classes from "./style.module.scss";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

const AboutMenu = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  const goTo = (url) => {
    navigate(url);
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop5" />
      <Decoration type="dbot8" />
      <TopNavigation title={wording.aboutGame} />
      <div className={classes.menuWrapper}>
        <div className={classes.materi1} onClick={() => goTo("/about/profile")}>
          <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.profile }} />
        </div>
        <div className={classes.materi2} onClick={() => goTo("/about/description")}>
          <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.description }} />
        </div>
        <div className={classes.materi3} onClick={() => goTo("/about/goal")}>
          <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.goal }} />
        </div>
      </div>
    </div>
  );
};

export default AboutMenu;
