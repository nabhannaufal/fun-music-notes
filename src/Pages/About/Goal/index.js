import React, { useEffect } from "react";

import { wording } from "Wording";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

const Goal = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);
  return (
    <div className={classes.content}>
      <Decoration type="dtop7" />
      <Decoration type="dbot10" />
      <TopNavigation />
      <div className={classes.menuWrapper}>
        <div className={classes.materi}>
          <div className={classes.materiTitle} dangerouslySetInnerHTML={{ __html: wording.goal }} />
          <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.goalText }} />
        </div>
      </div>
    </div>
  );
};

export default Goal;
