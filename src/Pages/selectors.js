import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectStarterpackState = (state) => state.funNoteState || initialState;

const selectProfile = createSelector(selectStarterpackState, (state) => state.profile);
const selectIsSubmit = createSelector(selectStarterpackState, (state) => state.isSubmit);
const selectIsOpenHint = createSelector(selectStarterpackState, (state) => state.isOpenHint);
const selectStatusLevel = createSelector(selectStarterpackState, (state) => state.statusLevel);
const selectLevel1Config = createSelector(selectStarterpackState, (state) => state.level1Config);
const selectLevel1Data = createSelector(selectStarterpackState, (state) => state.level1Data);
const selectLevel2Config = createSelector(selectStarterpackState, (state) => state.level2Config);
const selectLevel2Data = createSelector(selectStarterpackState, (state) => state.level2Data);
const selectLevel3Config = createSelector(selectStarterpackState, (state) => state.level3Config);
const selectLevel3Data = createSelector(selectStarterpackState, (state) => state.level3Data);
const selectLevel4Config = createSelector(selectStarterpackState, (state) => state.level4Config);
const selectLevel4Data = createSelector(selectStarterpackState, (state) => state.level4Data);

export {
  selectProfile,
  selectIsSubmit,
  selectIsOpenHint,
  selectStatusLevel,
  selectLevel1Config,
  selectLevel1Data,
  selectLevel2Config,
  selectLevel2Data,
  selectLevel3Config,
  selectLevel3Data,
  selectLevel4Config,
  selectLevel4Data,
};
