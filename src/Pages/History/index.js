import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";
import isEmpty from "lodash/isEmpty";
import { setMuteBacksound } from "Containers/App/actions";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";
import { wording } from "Wording";

import { selectLevel1Data, selectLevel2Data, selectLevel3Data, selectLevel4Data } from "Pages/selectors";

import classes from "./style.module.scss";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

const History = ({ level1Data, level2Data, level3Data, level4Data }) => {
  const dispatch = useDispatch();
  const [openLevel1, setOpenLevel1] = useState(false);
  const [openLevel2, setOpenLevel2] = useState(false);
  const [openLevel3, setOpenLevel3] = useState(false);
  const [openLevel4, setOpenLevel4] = useState(false);

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  const genMenu = (levelData, openLevel, setOpenLevel, title) => {
    return (
      <div className={classes.menu}>
        <div className={classes.navMenu} onClick={() => setOpenLevel(!openLevel)} style={{ cursor: "pointer" }}>
          <div className={classes.text}>{title}</div>
          <ExpandMoreIcon />
        </div>

        <Collapse in={openLevel}>
          <div className={classes.navContent}>
            {!isEmpty(levelData?.historyScore) ? (
              <table>
                <thead>
                  <tr>
                    <th>{wording.number}</th>
                    <th>{wording.score}</th>
                    <th>{wording.date}</th>
                  </tr>
                </thead>
                <tbody>
                  {levelData.historyScore.map((item, index) => {
                    return (
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.score}</td>
                        <td>{item.timeStamp}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            ) : (
              wording.noData
            )}
          </div>
        </Collapse>
      </div>
    );
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop9" />
      <Decoration type="dbot12" />
      <TopNavigation title={wording.historyScore} />
      <div className={classes.menuWrapper}>
        {genMenu(level1Data, openLevel1, setOpenLevel1, wording.titlelevel1)}
        {genMenu(level2Data, openLevel2, setOpenLevel2, wording.titlelevel2)}
        {genMenu(level3Data, openLevel3, setOpenLevel3, wording.titlelevel3)}
        {genMenu(level4Data, openLevel4, setOpenLevel4, wording.titlelevel4)}
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  level1Data: selectLevel1Data,
  level2Data: selectLevel2Data,
  level3Data: selectLevel3Data,
  level4Data: selectLevel4Data,
});

History.propTypes = {
  level4Config: PropTypes.object,
  level1Data: PropTypes.object,
  level2Data: PropTypes.object,
  level3Data: PropTypes.object,
  level4Data: PropTypes.object,
};

export default connect(mapStateToProps)(History);
