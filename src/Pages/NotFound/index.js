import React from "react";
import { wording } from "Wording";

import classes from "./style.module.scss";

const NotFound = () => {
  return (
    <div className={classes.wrapper}>
      <div className={classes.wrapper}>{wording.notFound}&#128549;</div>
    </div>
  );
};

export default NotFound;
