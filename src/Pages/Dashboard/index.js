import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import { connect, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import { wording, url } from "Wording";

import { logout } from "Pages/actions";
import { selectIsOpenHint, selectProfile } from "Pages/selectors";
import { selectPlayBacksound, selectOpenHint } from "Containers/App/selectors";
import { setPlayBacksound, setMuteBacksound, setOpenHint } from "Containers/App/actions";
import { setHint } from "Pages/actions";

import PopupExit from "Components/PopupExit";
import PopupHint from "Components/PopupHint";

import sound from "Images/sound.png";
import soundmute from "Images/sound-mute.png";
import game from "Images/game.webp";
import materi from "Images/materi.webp";
import hint from "Images/hint.webp";
import about from "Images/about.webp";
import history from "Images/history.webp";
import survey from "Images/survey.webp";
import exit from "Images/exit.webp";

import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

const Dashboard = ({ profile, playBacksound, openHint, isOpenHint }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [openExitPopup, setOpenExitPopup] = useState(false);

  useEffect(() => {
    if (isEmpty(profile)) {
      navigate("/dashboard");
    }
  }, [profile, navigate]);

  useEffect(() => {
    if (!isOpenHint) {
      dispatch(setOpenHint(true));
    }
  }, [isOpenHint, dispatch]);

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  const onExit = () => {
    dispatch(logout());
    navigate("/");
  };

  const goTo = (url) => {
    navigate(url);
  };

  const onSound = () => {
    dispatch(setPlayBacksound(!playBacksound));
  };

  const onClickSurvey = () => {
    dispatch(setMuteBacksound(true));
    window.open(url.survey, "_blank", "noopener,noreferrer");
  };

  const genMenu = (urlImg, text, onClick) => (
    <div className={classes.menuWrapper} onClick={onClick}>
      <img alt={wording.images} className={classes.iconMenu} src={urlImg} />
      <div className={classes.menuText}>{text}</div>
    </div>
  );

  const onClose = () => {
    dispatch(setOpenHint(false));
    dispatch(setHint(true));
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop3" />
      <Decoration type="dbot3" />
      <div className={classes.navigationTop}>
        <div className={classes.navigation} onClick={onSound}>
          <img alt={wording.images} className={classes.iconSound} src={playBacksound ? sound : soundmute} />
        </div>
      </div>

      <div className={classes.mainContent}>
        <div className={classes.title}>{wording.title}</div>
        <div className={classes.menu}>
          {genMenu(game, wording.game, () => goTo("/game"))}
          {genMenu(materi, wording.materi, () => goTo("/materi"))}
        </div>
        <div className={classes.menu}>
          {genMenu(hint, wording.hint, () => goTo("/hint"))}
          {genMenu(about, wording.about, () => goTo("/about"))}
        </div>
        <div className={classes.menu}>
          {genMenu(history, wording.history, () => goTo("/history"))}
          {genMenu(survey, wording.survey, onClickSurvey)}
        </div>
      </div>
      <div className={classes.navigationBottom}>
        <div className={classes.navigation} onClick={() => setOpenExitPopup(true)}>
          <img alt={wording.images} className={classes.iconExit} src={exit} />
        </div>
      </div>
      <PopupExit
        open={openExitPopup}
        title={wording.promptExitGame}
        onClose={() => setOpenExitPopup(false)}
        onClickNo={() => setOpenExitPopup(false)}
        onClickYes={onExit}
      />
      <PopupHint open={openHint} onClose={onClose} />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  profile: selectProfile,
  playBacksound: selectPlayBacksound,
  openHint: selectOpenHint,
  isOpenHint: selectIsOpenHint,
});

Dashboard.propTypes = {
  profile: PropTypes.object,
  playBacksound: PropTypes.bool,
  openHint: PropTypes.bool,
  isOpenHint: PropTypes.bool,
};

export default connect(mapStateToProps)(Dashboard);
