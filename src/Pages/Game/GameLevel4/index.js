import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { wording } from "Wording";
import { quest4 } from "Questions/quest4";

import { sendData } from "Containers/App/actions";
import { selectLevel4Config, selectLevel4Data, selectProfile } from "Pages/selectors";
import { setLevel4Config, resetLevel4Config, setLevel4Data } from "Pages/actions";
import { useBackListener } from "Utils/useBackListener";

import GameBoard from "Components/GameBoard";
import Decoration from "Components/Decoration";
import TopNavigation from "Components/TopNavigation";
import PopupAnswerKey from "Components/PopupAnswerKey";
import PopupCompleted from "Components/PopupCompleted";

import classes from "./style.module.scss";

const GameLevel4 = ({ level4Config, level4Data, profile }) => {
  const [openAnswerKey, setOpenAnswerKey] = useState(false);
  const dispatch = useDispatch();
  const [openPopup, setOpenPopup] = useState(false);

  const onSendData = (levelScore, levelTimestamp) => {
    const row = {
      name: profile.name,
      email: profile.email,
      score: levelScore,
      timestamp: levelTimestamp,
      level: wording.level4,
    };
    dispatch(
      sendData(row, "level4", () => {
        setOpenPopup(true);
      })
    );
  };

  useBackListener();
  return (
    <div className={classes.wrapper}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level4Title} isMateri noBackButton />
      <GameBoard
        levelConfig={level4Config}
        levelData={level4Data}
        quest={quest4}
        setLevelConfig={setLevel4Config}
        setLevelData={setLevel4Data}
        resetLevelConfig={resetLevel4Config}
        title={wording.level4Title}
        level={wording.level4}
        openAnswerKey={() => setOpenAnswerKey(true)}
        onSendData={onSendData}
        isRenderSound
      />
      <PopupCompleted
        open={openPopup}
        message={wording.levelCompletedtext}
        title={wording.levelCompleted}
        btnPrimaryText={wording.close}
        onClose={() => setOpenPopup(false)}
        onClickBtnPrimary={() => setOpenPopup(false)}
      />
      <PopupAnswerKey
        open={openAnswerKey}
        title={wording.answerKey}
        onClose={() => setOpenAnswerKey(false)}
        type="level4"
      />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  level4Config: selectLevel4Config,
  level4Data: selectLevel4Data,
  profile: selectProfile,
});

GameLevel4.propTypes = {
  level4Config: PropTypes.object,
  level4Data: PropTypes.object,
  profile: PropTypes.object,
};

export default connect(mapStateToProps)(GameLevel4);
