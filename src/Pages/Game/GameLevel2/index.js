import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { wording } from "Wording";
import { quest2 } from "Questions/quest2";
import { sendData } from "Containers/App/actions";
import { selectLevel2Config, selectLevel2Data, selectStatusLevel, selectProfile } from "Pages/selectors";
import { setLevel2Config, resetLevel2Config, setLevel2Data, setStatusLevel } from "Pages/actions";
import { useBackListener } from "Utils/useBackListener";

import GameBoard from "Components/GameBoard";
import Decoration from "Components/Decoration";
import TopNavigation from "Components/TopNavigation";
import PopupAnswerKey from "Components/PopupAnswerKey";

import classes from "./style.module.scss";

const GameLevel2 = ({ level2Config, level2Data, statusLevel, profile }) => {
  const [openAnswerKey, setOpenAnswerKey] = useState(false);
  const dispatch = useDispatch();
  const onNextUnlockLevel = () => {
    dispatch(
      setStatusLevel({
        ...statusLevel,
        level3: true,
      })
    );
  };

  const onSendData = (levelScore, levelTimestamp) => {
    const row = {
      name: profile.name,
      email: profile.email,
      score: levelScore,
      timestamp: levelTimestamp,
      level: wording.level2,
    };
    dispatch(sendData(row, "level2"));
  };

  useBackListener();
  return (
    <div className={classes.wrapper}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level2Title} isMateri noBackButton />
      <GameBoard
        levelConfig={level2Config}
        levelData={level2Data}
        quest={quest2}
        setLevelConfig={setLevel2Config}
        setLevelData={setLevel2Data}
        resetLevelConfig={resetLevel2Config}
        title={wording.level2Title}
        level={wording.level2}
        nextUnlockLevel={onNextUnlockLevel}
        openAnswerKey={() => setOpenAnswerKey(true)}
        onSendData={onSendData}
      />
      <PopupAnswerKey
        open={openAnswerKey}
        title={wording.answerKey}
        onClose={() => setOpenAnswerKey(false)}
        type="level2"
      />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  level2Config: selectLevel2Config,
  level2Data: selectLevel2Data,
  statusLevel: selectStatusLevel,
  profile: selectProfile,
});

GameLevel2.propTypes = {
  level2Config: PropTypes.object,
  level2Data: PropTypes.object,
  statusLevel: PropTypes.object,
  profile: PropTypes.object,
};

export default connect(mapStateToProps)(GameLevel2);
