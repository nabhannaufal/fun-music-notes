import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { wording } from "Wording";
import { quest1 } from "Questions/quest1";
import { sendData } from "Containers/App/actions";
import { selectLevel1Config, selectLevel1Data, selectStatusLevel, selectProfile } from "Pages/selectors";
import { setLevel1Config, resetLevel1Config, setLevel1Data, setStatusLevel } from "Pages/actions";
import { useBackListener } from "Utils/useBackListener";

import GameBoard from "Components/GameBoard";
import Decoration from "Components/Decoration";
import TopNavigation from "Components/TopNavigation";
import PopupAnswerKey from "Components/PopupAnswerKey";

import classes from "./style.module.scss";

const GameLevel1 = ({ level1Config, level1Data, statusLevel, profile }) => {
  const [openAnswerKey, setOpenAnswerKey] = useState(false);
  const dispatch = useDispatch();
  const onNextUnlockLevel = () => {
    dispatch(
      setStatusLevel({
        ...statusLevel,
        level2: true,
      })
    );
  };

  const onSendData = (levelScore, levelTimestamp) => {
    const row = {
      name: profile.name,
      email: profile.email,
      score: levelScore,
      timestamp: levelTimestamp,
      level: wording.level1,
    };
    dispatch(sendData(row, "level1"));
  };

  useBackListener();
  return (
    <div className={classes.wrapper}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level1Title} isMateri noBackButton />
      <GameBoard
        levelConfig={level1Config}
        levelData={level1Data}
        quest={quest1}
        setLevelConfig={setLevel1Config}
        setLevelData={setLevel1Data}
        resetLevelConfig={resetLevel1Config}
        title={wording.level1Title}
        level={wording.level1}
        nextUnlockLevel={onNextUnlockLevel}
        openAnswerKey={() => setOpenAnswerKey(true)}
        onSendData={onSendData}
      />
      <PopupAnswerKey
        open={openAnswerKey}
        title={wording.answerKey}
        onClose={() => setOpenAnswerKey(false)}
        type="level1"
      />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  level1Config: selectLevel1Config,
  level1Data: selectLevel1Data,
  statusLevel: selectStatusLevel,
  profile: selectProfile,
});

GameLevel1.propTypes = {
  level1Config: PropTypes.object,
  level1Data: PropTypes.object,
  statusLevel: PropTypes.object,
  profile: PropTypes.object,
};

export default connect(mapStateToProps)(GameLevel1);
