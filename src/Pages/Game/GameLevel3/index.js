import React, { useState } from "react";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { wording } from "Wording";
import { quest3 } from "Questions/quest3";
import { sendData } from "Containers/App/actions";
import { selectLevel3Config, selectLevel3Data, selectStatusLevel, selectProfile } from "Pages/selectors";
import { setLevel3Config, resetLevel3Config, setLevel3Data, setStatusLevel } from "Pages/actions";
import { useBackListener } from "Utils/useBackListener";

import GameBoard from "Components/GameBoard";
import Decoration from "Components/Decoration";
import TopNavigation from "Components/TopNavigation";
import PopupAnswerKey from "Components/PopupAnswerKey";

import classes from "./style.module.scss";

const GameLevel3 = ({ level3Config, level3Data, statusLevel, profile }) => {
  const [openAnswerKey, setOpenAnswerKey] = useState(false);
  const dispatch = useDispatch();
  const onNextUnlockLevel = () => {
    dispatch(
      setStatusLevel({
        ...statusLevel,
        level4: true,
      })
    );
  };

  const onSendData = (levelScore, levelTimestamp) => {
    const row = {
      name: profile.name,
      email: profile.email,
      score: levelScore,
      timestamp: levelTimestamp,
      level: wording.level3,
    };
    dispatch(sendData(row, "level3"));
  };

  useBackListener();
  return (
    <div className={classes.wrapper}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level3Title} isMateri noBackButton />
      <GameBoard
        levelConfig={level3Config}
        levelData={level3Data}
        quest={quest3}
        setLevelConfig={setLevel3Config}
        setLevelData={setLevel3Data}
        resetLevelConfig={resetLevel3Config}
        title={wording.level3Title}
        level={wording.level3}
        nextUnlockLevel={onNextUnlockLevel}
        openAnswerKey={() => setOpenAnswerKey(true)}
        onSendData={onSendData}
      />
      <PopupAnswerKey
        open={openAnswerKey}
        title={wording.answerKey}
        onClose={() => setOpenAnswerKey(false)}
        type="level3"
      />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  level3Config: selectLevel3Config,
  level3Data: selectLevel3Data,
  statusLevel: selectStatusLevel,
  profile: selectProfile,
});

GameLevel3.propTypes = {
  level3Config: PropTypes.object,
  level3Data: PropTypes.object,
  statusLevel: PropTypes.object,
  profile: PropTypes.object,
};

export default connect(mapStateToProps)(GameLevel3);
