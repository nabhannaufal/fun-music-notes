import React, { useEffect } from "react";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import { useNavigate } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { selectProfile } from "Pages/selectors";
import { setPlayBacksound, setMuteBacksound } from "Containers/App/actions";

import { wording } from "Wording";

import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import guitar from "Images/guitar.gif";
import kecrek from "Images/kecrek.gif";
import homeIcon from "Images/homeIcon.png";

const Home = ({ profile }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onStart = () => {
    if (isEmpty(profile)) {
      navigate("/biodata");
    } else {
      navigate("/dashboard");
    }
    dispatch(setPlayBacksound(true));
  };

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  const renderLanding = () => (
    <div className={classes.wrapper}>
      <Decoration type="dtop1" />
      <Decoration type="dbot1" />
      <div className={classes.wrapperContent}>
        <div className={classes.title}>{wording.title}</div>
        <div className={classes.content}>
          <img className={classes.contentLeft} alt={wording.images} src={guitar} />
          <div className={classes.contentCenter}>
            <img className={classes.mainIcon} alt={wording.images} src={homeIcon} />
            <div className={classes.subtitle} onClick={onStart}>
              {wording.startgame}
            </div>
            <div className={classes.credit}>{wording.credit}</div>
          </div>
          <img className={classes.contentRight} alt={wording.images} src={kecrek} />
        </div>
      </div>
    </div>
  );

  return renderLanding();
};

const mapStateToProps = createStructuredSelector({
  profile: selectProfile,
});

Home.propTypes = {
  profile: PropTypes.object,
};

export default connect(mapStateToProps)(Home);
