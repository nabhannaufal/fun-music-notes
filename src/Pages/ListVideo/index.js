import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { wording } from "Wording";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import drum2 from "Images/drum2.gif";
import saxophone from "Images/saxophone.gif";
import piano from "Images/piano.gif";

const ListVideo = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  const goTo = (url) => {
    navigate(url);
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot7" />
      <TopNavigation title={wording.videoList} isGame />
      <div className={classes.menuWrapper}>
        <div className={classes.materiWrapper}>
          <div className={classes.materi1} onClick={() => goTo("/video/1")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.video1 }} />
          </div>
        </div>
        <div className={classes.materiWrapper}>
          <img alt={wording.images} className={classes.materiDecor1} src={piano} />
          <div className={classes.materi2} onClick={() => goTo("/video/2")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.video2 }} />
          </div>
        </div>
        <div className={classes.materiWrapper}>
          <div className={classes.materi3} onClick={() => goTo("/video/3")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.video3 }} />
          </div>
          <img alt={wording.images} className={classes.materiDecor2} src={drum2} />
        </div>
        <div className={classes.materiWrapper}>
          <img alt={wording.images} className={classes.materiDecor3} src={saxophone} />
          <div className={classes.materi4} onClick={() => goTo("/video/4")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.video4 }} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListVideo;
