import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import { wording } from "Wording";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import biola from "Images/biola.gif";
import drum from "Images/drum.gif";
import terompet from "Images/terompet.gif";

const ListArticle = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  const goTo = (url) => {
    navigate(url);
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot6" />
      <TopNavigation title={wording.materiList} isGame />
      <div className={classes.menuWrapper}>
        <div className={classes.materiWrapper}>
          <div className={classes.materi1} onClick={() => goTo("/article/1")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.materi1 }} />
          </div>
          <img alt={wording.images} className={classes.materiDecor1} src={biola} />
        </div>
        <div className={classes.materiWrapper}>
          <img alt={wording.images} className={classes.materiDecor2} src={terompet} />
          <div className={classes.materi2} onClick={() => goTo("/article/2")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.materi2 }} />
          </div>
        </div>
        <div className={classes.materiWrapper}>
          <div className={classes.materi3} onClick={() => goTo("/article/3")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.materi3 }} />
          </div>
        </div>
        <div className={classes.materiWrapper}>
          <img alt={wording.images} className={classes.materiDecor3} src={drum} />
          <div className={classes.materi4} onClick={() => goTo("/article/4")}>
            <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: wording.materi4 }} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ListArticle;
