import React from "react";

import { wording } from "Wording";

import TopNavigation from "Components/TopNavigation";
import ArticleMenu from "Components/ArticleMenu";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import materi4S1 from "Images/materi/materi4S1.PNG";
import materi4S2 from "Images/materi/materi4S2.PNG";
import materi4S3 from "Images/materi/materi4S3.PNG";

const ArticleLevel4 = () => {
  const materi = [{ img: materi4S1 }, { img: materi4S2 }, { img: materi4S3, sound: [{ id: 6, style: "default" }] }];

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level4Title} />
      <ArticleMenu materi={materi} />
    </div>
  );
};

export default ArticleLevel4;
