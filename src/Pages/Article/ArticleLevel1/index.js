import React from "react";

import { wording } from "Wording";

import TopNavigation from "Components/TopNavigation";
import ArticleMenu from "Components/ArticleMenu";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import materi1S1 from "Images/materi/materi1S1.PNG";
import materi1S2 from "Images/materi/materi1S2.PNG";
import materi1S3 from "Images/materi/materi1S3.PNG";
import materi1S4 from "Images/materi/materi1S4.PNG";
import materi1S5 from "Images/materi/materi1S5.PNG";
import materi1S6 from "Images/materi/materi1S6.PNG";
import materi1S7 from "Images/materi/materi1S7.PNG";
import materi1S8 from "Images/materi/materi1S8.PNG";
import materi1S9 from "Images/materi/materi1S9.PNG";
import materi1S10 from "Images/materi/materi1S10.PNG";

const ArticleLevel1 = () => {
  const materi = [
    { img: materi1S1 },
    { img: materi1S2 },
    { img: materi1S3, sound: [{ id: 0, style: "default" }] },
    { img: materi1S4, sound: [{ id: 1, style: "default" }] },
    {
      img: materi1S5,
      sound: [
        { id: 0, style: "default1" },
        { id: 2, style: "default2" },
      ],
    },
    { img: materi1S6, sound: [{ id: 3, style: "default" }] },
    { img: materi1S7 },
    { img: materi1S8 },
    { img: materi1S9 },
    { img: materi1S10 },
  ];

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level1Title} />
      <ArticleMenu materi={materi} />
    </div>
  );
};

export default ArticleLevel1;
