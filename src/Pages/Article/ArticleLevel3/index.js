import React from "react";

import { wording } from "Wording";

import TopNavigation from "Components/TopNavigation";
import ArticleMenu from "Components/ArticleMenu";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import materi3S1 from "Images/materi/materi3S1.PNG";
import materi3S2 from "Images/materi/materi3S2.PNG";
import materi3S3 from "Images/materi/materi3S3.PNG";
import materi3S4 from "Images/materi/materi3S4.PNG";
import materi3S5 from "Images/materi/materi3S5.PNG";
import materi3S6 from "Images/materi/materi3S6.PNG";
import materi3S7 from "Images/materi/materi3S7.PNG";
import materi3S8 from "Images/materi/materi3S8.PNG";
import materi3S9 from "Images/materi/materi3S9.PNG";
import materi3S10 from "Images/materi/materi3S10.PNG";
import materi3S11 from "Images/materi/materi3S11.PNG";
import materi3S12 from "Images/materi/materi3S12.PNG";
import materi3S13 from "Images/materi/materi3S13.PNG";
import materi3S14 from "Images/materi/materi3S14.PNG";
import materi3S15 from "Images/materi/materi3S15.PNG";
import materi3S16 from "Images/materi/materi3S16.PNG";
import materi3S17 from "Images/materi/materi3S17.PNG";

const ArticleLevel3 = () => {
  const materi = [
    { img: materi3S1 },
    { img: materi3S2 },
    { img: materi3S3 },
    { img: materi3S4 },
    { img: materi3S5 },
    { img: materi3S6 },
    { img: materi3S7 },
    { img: materi3S8 },
    { img: materi3S9 },
    { img: materi3S10 },
    { img: materi3S11 },
    { img: materi3S12 },
    { img: materi3S13 },
    { img: materi3S14 },
    { img: materi3S15 },
    { img: materi3S17 },
    { img: materi3S16 },
  ];

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level3Title} />
      <ArticleMenu materi={materi} />
    </div>
  );
};

export default ArticleLevel3;
