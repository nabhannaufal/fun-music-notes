import React from "react";

import { wording } from "Wording";

import TopNavigation from "Components/TopNavigation";
import ArticleMenu from "Components/ArticleMenu";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import materi2S1 from "Images/materi/materi2S1.PNG";
import materi2S2 from "Images/materi/materi2S2.PNG";
import materi2S3 from "Images/materi/materi2S3.PNG";
import materi2S4 from "Images/materi/materi2S4.PNG";
import materi2S5 from "Images/materi/materi2S5.PNG";
import materi2S6 from "Images/materi/materi2S6.PNG";
import materi2S7 from "Images/materi/materi2S7.PNG";

const ArticleLevel2 = () => {
  const materi = [
    { img: materi2S1 },
    { img: materi2S2 },
    {
      img: materi2S3,
      sound: [
        { id: 4, style: "default3" },
        { id: 5, style: "default4" },
      ],
    },
    { img: materi2S4 },
    { img: materi2S5 },
    { img: materi2S6 },
    { img: materi2S7 },
  ];

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level2Title} />
      <ArticleMenu materi={materi} />
    </div>
  );
};

export default ArticleLevel2;
