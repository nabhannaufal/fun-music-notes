import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";
import { setMuteBacksound } from "Containers/App/actions";
import { selectStatusLevel } from "Pages/selectors";

import { wording } from "Wording";

import Decoration from "Components/Decoration";
import TopNavigation from "Components/TopNavigation";

import classes from "./style.module.scss";

import note1 from "Images/note1.webp";
import note2 from "Images/note2.webp";
import note3 from "Images/note3.webp";
import note4 from "Images/note4.webp";
import lock from "Images/lock.webp";
import saxophone from "Images/saxophone.gif";
import ketok from "Images/ketok.gif";
import guitar2 from "Images/guitar2.gif";

const ListGame = ({ statusLevel }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  const goTo = (url) => {
    navigate(url);
  };

  const renderMateri = (style, lockedStyle, urlImg, text, onClick, isUnlocked) => {
    if (isUnlocked) {
      return (
        <div className={style} onClick={onClick}>
          <img className={classes.materiIcon} alt={wording.images} src={urlImg} />
          <div className={classes.materiText} dangerouslySetInnerHTML={{ __html: text }} />
        </div>
      );
    }
    return (
      <div className={lockedStyle}>
        <img className={classes.lockIcon} alt={wording.images} src={lock} />
      </div>
    );
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot4" />
      <TopNavigation title={wording.gameList} isMateri backToDashboard />
      <div className={classes.menuWrapper}>
        <div className={classes.materiWrapper}>
          {renderMateri(
            classes.materi1,
            classes.materi1Locked,
            note1,
            wording.level1,
            () => goTo("/game/level1"),
            statusLevel?.level1
          )}
          <div className={classes.waveBottom} />
        </div>
        <div className={classes.materiWrapper}>
          <img className={classes.saxophone} alt={wording.images} src={saxophone} />
          <div className={classes.waveTop} />
          {renderMateri(
            classes.materi2,
            classes.materi2Locked,
            note2,
            wording.level2,
            () => goTo("/game/level2"),
            statusLevel?.level2
          )}
        </div>
        <div className={classes.materiWrapper}>
          {renderMateri(
            classes.materi3,
            classes.materi3Locked,
            note3,
            wording.level3,
            () => goTo("/game/level3"),
            statusLevel?.level3
          )}
          <div className={classes.waveTop2} />
          <img className={classes.guitar2} alt={wording.images} src={guitar2} />
        </div>
        <div className={classes.materiWrapper}>
          <img className={classes.ketok} alt={wording.images} src={ketok} />
          {renderMateri(
            classes.materi4,
            classes.materi4Locked,
            note4,
            wording.level4,
            () => goTo("/game/level4"),
            statusLevel?.level4
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  statusLevel: selectStatusLevel,
});

ListGame.propTypes = {
  statusLevel: PropTypes.object,
};

export default connect(mapStateToProps)(ListGame);
