import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { setMuteBacksound, setOpenHint } from "Containers/App/actions";
import { createStructuredSelector } from "reselect";
import { wording } from "Wording";

import { selectOpenHint } from "Containers/App/selectors";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";
import PopupHint from "Components/PopupHint";

import classes from "./style.module.scss";

import hint2 from "Images/hint2.webp";
import hint3 from "Images/hint3.webp";

const HintMenu = ({ openHint }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  const goTo = (url) => {
    navigate(url);
  };

  const onClose = () => {
    dispatch(setOpenHint(false));
  };

  const onOpen = () => {
    dispatch(setOpenHint(true));
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop4" />
      <Decoration type="dbot5" />
      <TopNavigation title={wording.hint} />
      <div className={classes.menuWrapper}>
        <div className={classes.materi} onClick={onOpen}>
          <img className={classes.materiIcon} alt={wording.images} src={hint2} />
          <div className={classes.materiText}>{wording.game}</div>
        </div>
        <div className={classes.materi} onClick={() => goTo("/hint/app")}>
          <img className={classes.videoIcon} alt={wording.images} src={hint3} />
          <div className={classes.materiText}>{wording.app}</div>
        </div>
      </div>
      <PopupHint open={openHint} onClose={onClose} />
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  openHint: selectOpenHint,
});

HintMenu.propTypes = {
  openHint: PropTypes.bool,
};

export default connect(mapStateToProps)(HintMenu);
