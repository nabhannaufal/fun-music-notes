import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import { wording } from "Wording";

import classes from "./style.module.scss";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import sound from "Images/sound.png";
import game from "Images/game.webp";
import materi from "Images/materi.webp";
import hint from "Images/hint.webp";
import about from "Images/about.webp";
import history from "Images/history.webp";
import survey from "Images/survey.webp";
import exit from "Images/exit.webp";

const HintApp = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);
  const genMenu = (img, text) => {
    return (
      <div className={classes.menu}>
        <div className={classes.menuImg}>
          <img src={img} alt={wording.images} className={classes.img} />
        </div>
        <div className={classes.menuText}>{text}</div>
      </div>
    );
  };
  return (
    <div className={classes.content}>
      <Decoration type="dtop8" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.hint} />
      <div className={classes.menuWrapper}>
        {genMenu(game, wording.gameHint)}
        {genMenu(hint, wording.hintHint)}
        {genMenu(history, wording.historyHint)}
        {genMenu(sound, wording.soundHint)}
        {genMenu(materi, wording.materiHint)}
        {genMenu(about, wording.aboutHint)}
        {genMenu(survey, wording.surveyHint)}
        {genMenu(exit, wording.exitHint)}
      </div>
    </div>
  );
};

export default HintApp;
