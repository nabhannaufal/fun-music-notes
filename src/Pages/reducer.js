import produce from "immer";

import {
  FN_SET_PROFILE,
  FN_SET_LEVEL1_CONFIG,
  FN_SET_LEVEL1_DATA,
  FN_RESET_LEVEL1_CONFIG,
  FN_SET_LEVEL2_CONFIG,
  FN_SET_LEVEL2_DATA,
  FN_RESET_LEVEL2_CONFIG,
  FN_SET_LEVEL3_CONFIG,
  FN_SET_LEVEL3_DATA,
  FN_RESET_LEVEL3_CONFIG,
  FN_SET_LEVEL4_CONFIG,
  FN_SET_LEVEL4_DATA,
  FN_RESET_LEVEL4_CONFIG,
  FN_LOGOUT,
  FN_SET_STATUS_LEVEL,
  FN_SET_SUBMIT,
  FN_SET_HINT,
} from "./constants";

export const initialState = {
  profile: null,
  isSubmit: false,
  isOpenHint: false,
  statusLevel: {
    level1: true,
    level2: false,
    level3: false,
    level4: false,
    sendData: false,
  },
  level1Config: {
    answer: [
      {
        no: 1,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 2,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 3,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 4,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 5,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 6,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 7,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 8,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 9,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 10,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
    ],
    totelScore: 0,
    isCompleted: false,
    currentIndex: 0,
    openBoard: false,
  },
  level1Data: {
    firstAnswer: null,
    currentScore: 0,
    timeStamp: null,
    historyScore: [],
  },
  level2Config: {
    answer: [
      {
        no: 1,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 2,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 3,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 4,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 5,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 6,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 7,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 8,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 9,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 10,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
    ],
    totelScore: 0,
    isCompleted: false,
    currentIndex: 0,
    openBoard: false,
  },
  level2Data: {
    firstAnswer: null,
    currentScore: 0,
    timeStamp: null,
    historyScore: [],
  },
  level3Config: {
    answer: [
      {
        no: 1,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 2,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 3,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 4,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 5,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 6,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 7,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 8,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 9,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 10,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
    ],
    totelScore: 0,
    isCompleted: false,
    currentIndex: 0,
    openBoard: false,
  },
  level3Data: {
    firstAnswer: null,
    currentScore: 0,
    timeStamp: null,
    historyScore: [],
  },
  level4Config: {
    answer: [
      {
        no: 1,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 2,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 3,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 4,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 5,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 6,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 7,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 8,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 9,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
      {
        no: 10,
        answer: null,
        result: null,
        lockAnswer: false,
        score: 0,
      },
    ],
    totelScore: 0,
    isCompleted: false,
    currentIndex: 0,
    openBoard: false,
  },
  level4Data: {
    firstAnswer: null,
    timeStamp: null,
    currentScore: 0,
    historyScore: [],
  },
};

export const storedKey = [
  "profile",
  "isSubmit",
  "isOpenHint",
  "statusLevel",
  "level1Config",
  "level1Data",
  "level2Config",
  "level2Data",
  "level3Config",
  "level3Data",
  "level4Config",
  "level4Data",
];

/* eslint-disable default-case, no-param-reassign */
const funNoteReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case FN_LOGOUT:
        draft.profile = initialState.profile;
        draft.isSubmit = initialState.isSubmit;
        draft.isOpenHint = initialState.isOpenHint;
        draft.statusLevel = initialState.statusLevel;
        draft.level1Config = initialState.level1Config;
        draft.level1Data = initialState.level1Data;
        draft.level2Config = initialState.level2Config;
        draft.level2Data = initialState.level2Data;
        draft.level3Config = initialState.level3Config;
        draft.level3Data = initialState.level3Data;
        draft.level4Config = initialState.level4Config;
        draft.level4Data = initialState.level4Data;
        break;
      case FN_SET_PROFILE:
        draft.profile = action.profile;
        break;
      case FN_SET_HINT:
        draft.isOpenHint = action.isOpenHint;
        break;
      case FN_SET_STATUS_LEVEL:
        draft.statusLevel = action.statusLevel;
        break;
      case FN_SET_LEVEL1_CONFIG:
        draft.level1Config = action.level1Config;
        break;
      case FN_SET_LEVEL1_DATA:
        draft.level1Data = action.level1Data;
        break;
      case FN_RESET_LEVEL1_CONFIG:
        draft.level1Config = initialState.level1Config;
        break;
      case FN_SET_LEVEL2_CONFIG:
        draft.level2Config = action.level2Config;
        break;
      case FN_SET_LEVEL2_DATA:
        draft.level2Data = action.level2Data;
        break;
      case FN_RESET_LEVEL2_CONFIG:
        draft.level2Config = initialState.level2Config;
        break;
      case FN_SET_LEVEL3_CONFIG:
        draft.level3Config = action.level3Config;
        break;
      case FN_SET_LEVEL3_DATA:
        draft.level3Data = action.level3Data;
        break;
      case FN_RESET_LEVEL3_CONFIG:
        draft.level3Config = initialState.level3Config;
        break;
      case FN_SET_LEVEL4_CONFIG:
        draft.level4Config = action.level4Config;
        break;
      case FN_SET_LEVEL4_DATA:
        draft.level4Data = action.level4Data;
        break;
      case FN_RESET_LEVEL4_CONFIG:
        draft.level4Config = initialState.level4Config;
        break;
      case FN_SET_SUBMIT:
        draft.isSubmit = action.isSubmit;
        break;
    }
  });

export default funNoteReducer;
