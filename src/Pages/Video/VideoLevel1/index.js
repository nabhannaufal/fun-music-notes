import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { setMuteBacksound } from "Containers/App/actions";

import { wording, url } from "Wording";

import TopNavigation from "Components/TopNavigation";
import VideoBoard from "Components/VideoBoard";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

const VideoLevel1 = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level1Title} />
      <VideoBoard url={url.video1} />
    </div>
  );
};

export default VideoLevel1;
