import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { setMuteBacksound } from "Containers/App/actions";

import { wording, url } from "Wording";

import TopNavigation from "Components/TopNavigation";
import VideoBoard from "Components/VideoBoard";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";
const VideoLevel2 = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  return (
    <div className={classes.content}>
      <Decoration type="dtop" />
      <Decoration type="dbot1" />
      <TopNavigation title={wording.level2Title} />
      <VideoBoard url={url.video2} />
    </div>
  );
};

export default VideoLevel2;
