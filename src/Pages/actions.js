import {
  FN_SET_PROFILE,
  FN_SET_LEVEL1_CONFIG,
  FN_SET_LEVEL1_DATA,
  FN_RESET_LEVEL1_CONFIG,
  FN_SET_LEVEL2_CONFIG,
  FN_SET_LEVEL2_DATA,
  FN_RESET_LEVEL2_CONFIG,
  FN_SET_LEVEL3_CONFIG,
  FN_SET_LEVEL3_DATA,
  FN_RESET_LEVEL3_CONFIG,
  FN_SET_LEVEL4_CONFIG,
  FN_SET_LEVEL4_DATA,
  FN_RESET_LEVEL4_CONFIG,
  FN_LOGOUT,
  FN_SET_STATUS_LEVEL,
  FN_SET_SUBMIT,
  FN_SET_HINT,
} from "./constants";

export function setProfile(profile) {
  return {
    type: FN_SET_PROFILE,
    profile,
  };
}

export function setSubmit(isSubmit) {
  return {
    type: FN_SET_SUBMIT,
    isSubmit,
  };
}

export function setHint(isOpenHint) {
  return {
    type: FN_SET_HINT,
    isOpenHint,
  };
}

export function setStatusLevel(statusLevel) {
  return {
    type: FN_SET_STATUS_LEVEL,
    statusLevel,
  };
}

export function logout() {
  return {
    type: FN_LOGOUT,
  };
}

export function setLevel1Config(level1Config) {
  return {
    type: FN_SET_LEVEL1_CONFIG,
    level1Config,
  };
}

export function setLevel1Data(level1Data) {
  return {
    type: FN_SET_LEVEL1_DATA,
    level1Data,
  };
}

export function resetLevel1Config() {
  return {
    type: FN_RESET_LEVEL1_CONFIG,
  };
}
export function setLevel2Config(level2Config) {
  return {
    type: FN_SET_LEVEL2_CONFIG,
    level2Config,
  };
}

export function setLevel2Data(level2Data) {
  return {
    type: FN_SET_LEVEL2_DATA,
    level2Data,
  };
}

export function resetLevel2Config() {
  return {
    type: FN_RESET_LEVEL2_CONFIG,
  };
}

export function setLevel3Config(level3Config) {
  return {
    type: FN_SET_LEVEL3_CONFIG,
    level3Config,
  };
}

export function setLevel3Data(level3Data) {
  return {
    type: FN_SET_LEVEL3_DATA,
    level3Data,
  };
}

export function resetLevel3Config() {
  return {
    type: FN_RESET_LEVEL3_CONFIG,
  };
}

export function setLevel4Config(level4Config) {
  return {
    type: FN_SET_LEVEL4_CONFIG,
    level4Config,
  };
}

export function setLevel4Data(level4Data) {
  return {
    type: FN_SET_LEVEL4_DATA,
    level4Data,
  };
}

export function resetLevel4Config() {
  return {
    type: FN_RESET_LEVEL4_CONFIG,
  };
}
