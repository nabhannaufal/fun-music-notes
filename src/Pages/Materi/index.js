import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import { wording } from "Wording";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

import materi from "Images/materi.webp";
import video from "Images/video.webp";

const Materi = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  const goTo = (url) => {
    navigate(url);
  };

  return (
    <div className={classes.content}>
      <Decoration type="dtop4" />
      <Decoration type="dbot5" />
      <TopNavigation title={wording.materiType} isGame backToDashboard />
      <div className={classes.menuWrapper}>
        <div className={classes.materi} onClick={() => goTo("/article")}>
          <img className={classes.materiIcon} alt={wording.images} src={materi} />
          <div className={classes.materiText}>{wording.article}</div>
        </div>
        <div className={classes.materi} onClick={() => goTo("/video")}>
          <img className={classes.videoIcon} alt={wording.images} src={video} />
          <div className={classes.materiText}>{wording.video}</div>
        </div>
      </div>
    </div>
  );
};

export default Materi;
