import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import classNames from "classnames";
import { useNavigate } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { setProfile } from "Pages/actions";
import { selectProfile } from "Pages/selectors";
import { setMuteBacksound } from "Containers/App/actions";

import { wording } from "Wording";
import { validationName, validationEmail } from "Utils/userInputValidation";

import TopNavigation from "Components/TopNavigation";
import Decoration from "Components/Decoration";

import classes from "./style.module.scss";

const Biodata = ({ profile }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [name, setName] = useState(profile?.name || "");
  const [email, setEmail] = useState(profile?.email || "");
  const [errorName, setErrorName] = useState("");
  const [errorMail, setErrorMail] = useState("");
  const [validForm, setvalidForm] = useState(false);

  const handleName = (e) => {
    const { value } = e.target;
    setName(value);
    setErrorName(validationName(value));
  };

  const handleEmail = (e) => {
    const { value } = e.target;
    setEmail(value);
    setErrorMail(validationEmail(value));
  };

  const onSubmit = () => {
    const newProfile = {
      name,
      email,
    };
    dispatch(setProfile(newProfile));
    navigate("/dashboard");
  };

  useEffect(() => {
    if (!isEmpty(name) && !isEmpty(email) && isEmpty(errorName) && isEmpty(errorMail)) {
      setvalidForm(true);
    } else {
      setvalidForm(false);
    }
  }, [name, email, errorName, errorMail]);

  useEffect(() => {
    dispatch(setMuteBacksound(false));
  }, [dispatch]);

  return (
    <div className={classes.content}>
      <Decoration type="dtop2" />
      <Decoration type="dbot2" />
      <TopNavigation title={wording.biodata} />
      <div className={classes.form}>
        <div className={classes.formWrapper}>
          <form onSubmit={onSubmit}>
            <div className={classes.inputLabel}>{wording.name}</div>
            <input
              type="text"
              name={wording.name}
              placeholder={wording.namePlaceholder}
              value={name}
              onChange={handleName}
              className={classNames({
                [classes.inputError]: !isEmpty(errorName),
              })}
            />
            {!isEmpty(errorName) && <div className={classes.inputHelper}>{errorName}</div>}
            <div className={classes.inputLabel}>{wording.email}</div>
            <input
              type="email"
              name={wording.email}
              placeholder={wording.emailPlaceholder}
              value={email}
              onChange={handleEmail}
              className={classNames({
                [classes.inputError]: !isEmpty(errorMail),
              })}
            />
            {!isEmpty(errorMail) && <div className={classes.inputHelper}>{errorMail}</div>}
            <div className={classes.btnWrapper}>
              <button disabled={!validForm} className={classes.btnPrimary} type="submit">
                {isEmpty(profile) ? wording.continue : wording.update}
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  profile: selectProfile,
});

Biodata.propTypes = {
  profile: PropTypes.object,
};

export default connect(mapStateToProps)(Biodata);
