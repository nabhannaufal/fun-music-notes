import Home from "Pages/Home/loadable";
import NotFound from "Pages/NotFound/loadable";
import Biodata from "Pages/Biodata/loadable";
import Dashboard from "Pages/Dashboard/loadable";
import Materi from "Pages/Materi/loadable";
import ListArticle from "Pages/ListArticle/loadable";
import ListVideo from "Pages/ListVideo/loadable";
import ListGame from "Pages/ListGame/loadable";
import ArticleLevel1 from "Pages/Article/ArticleLevel1/loadable";
import ArticleLevel2 from "Pages/Article/ArticleLevel2/loadable";
import ArticleLevel3 from "Pages/Article/ArticleLevel3/loadable";
import ArticleLevel4 from "Pages/Article/ArticleLevel4/loadable";
import VideoLevel1 from "Pages/Video/VideoLevel1/loadable";
import VideoLevel2 from "Pages/Video/VideoLevel2/loadable";
import VideoLevel3 from "Pages/Video/VideoLevel3/loadable";
import VideoLevel4 from "Pages/Video/VideoLevel4/loadable";
import GameLevel1 from "Pages/Game/GameLevel1/loadable";
import GameLevel2 from "Pages/Game/GameLevel2/loadable";
import GameLevel3 from "Pages/Game/GameLevel3/loadable";
import GameLevel4 from "Pages/Game/GameLevel4/loadable";
import AboutMenu from "Pages/About/AboutMenu/loadable";
import Profile from "Pages/About/Profile/loadable";
import Description from "Pages/About/Description/loadable";
import Goal from "Pages/About/Goal/loadable";
import History from "Pages/History/loadable";
import HintMenu from "Pages/Hint/HintMenu/loadable";
import HintApp from "Pages/Hint/HintApp/loadable";

import HomeLayout from "Layout/HomeLayout";

const routes = [
  { path: "/", component: Home },
  { path: "/Biodata", component: Biodata },
  { path: "/dashboard", component: Dashboard },
  { path: "/materi", component: Materi },
  { path: "/history", component: History },
  {
    path: "/hint",
    subRoutes: [
      {
        path: "/",
        component: HintMenu,
      },
      {
        path: "/app",
        component: HintApp,
      },
    ],
  },
  {
    path: "/article",
    subRoutes: [
      {
        path: "/",
        component: ListArticle,
      },
      {
        path: "/1",
        component: ArticleLevel1,
      },
      {
        path: "/2",
        component: ArticleLevel2,
      },
      {
        path: "/3",
        component: ArticleLevel3,
      },
      {
        path: "/4",
        component: ArticleLevel4,
      },
    ],
  },
  {
    path: "/game",
    subRoutes: [
      {
        path: "/",
        component: ListGame,
      },
      {
        path: "/level1",
        component: GameLevel1,
      },
      {
        path: "/level2",
        component: GameLevel2,
      },
      {
        path: "/level3",
        component: GameLevel3,
      },
      {
        path: "/level4",
        component: GameLevel4,
      },
    ],
  },
  {
    path: "/video",
    subRoutes: [
      {
        path: "/",
        component: ListVideo,
      },
      {
        path: "/1",
        component: VideoLevel1,
      },
      {
        path: "/2",
        component: VideoLevel2,
      },
      {
        path: "/3",
        component: VideoLevel3,
      },
      {
        path: "/4",
        component: VideoLevel4,
      },
    ],
  },
  {
    path: "/about",
    subRoutes: [
      { path: "/", component: AboutMenu },
      { path: "/profile", component: Profile },
      { path: "/description", component: Description },
      { path: "/goal", component: Goal },
    ],
  },

  { path: "*", component: NotFound, layout: HomeLayout },
];

export default routes;
