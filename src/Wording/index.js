export const wording = {
  images: "gambar",
  notFound: "Maaf, halaman ini tidak ditemukan",
  startgame: "KLIK UNTUK MEMULAI GAME",
  title: "FUN MUSIC NOTES",
  credit: "OLEH: ASHILFASYA RAHMADHANI",
  biodataSubtitle: "Sebelum mulai, kenalan dulu yuk!",
  biodataUpdate: "Pastikan data yang kamu isi sudah sesuai ya",
  nextQuest: "Soal Selanjutnya",
  previousQuest: "Soal Sebelumnya",
  dashboard: "Dashboard",
  continue: "Lanjut",
  update: "Perbarui",
  back: "Kembali",
  yes: "Ya",
  no: "Tidak",
  name: "Nama",
  email: "Email",
  namePlaceholder: "Masukkan nama lengkap Kamu",
  emailPlaceholder: "Masukkan alamat email Kamu",
  errorEmpty: "Kolom ini tidak boleh kosong",
  errorEmail: "Masukan email yang valid",
  homeGreeting: "Hi, <strong>{name}</strong> Selamat datang di <span>Fun Music Notes</span>",
  game: "Permainan",
  app: "Aplikasi",
  materi: "Materi",
  biodata: "BIODATA",
  about: "Tentang",
  hint: "Petunjuk",
  historyScore: "Riwayat Skor",
  exit: "Keluar",
  survey: "Isi Survey",
  history: "Riwayat",
  quizSubtitle: "Ayo selesaikan setiap tahap permainan",
  level1: "Level 1",
  level2: "Level 2",
  level3: "Level 3",
  level4: "Level 4",
  sendData: "Kirim Data",
  level1Title: "TANDA BIRAMA & TANDA KUNCI",
  level2Title: "HARGA NADA & NAMA NADA",
  level3Title: "TANGGA NADA",
  level4Title: "RITMIS",
  levelLocked: "Terkunci",
  sendDataTitle: "Kirim nilai yang Kamu dapatkan",
  levelLockedtext: "Selesaikan tahap sebelumnya untuk membuka",
  correctAnswer: "Selamat! jawaban Kamu benar",
  incorrectAnswer: "Maaf jawaban Kamu belum tepat",
  incorrectAnswerText: "Jawaban yang kamu pilih masih belum benar, mohon perhatikan kembali soal yang diberikan",
  nextSoal: "Lanjut ke soal berikutnya",
  close: "Tutup",
  tryAgain: "Coba Lagi",
  nextLevel: "Level Selanjutnya",
  answerKey: "Kunci Jawaban",
  openAnswerKey: "Lihat kunci jawaban",
  checkAnswer: "Periksa Jawaban?",
  checkAgain: "Periksa Kembali",
  start: "Mulai",
  finish: "Selesai",
  promptExitGame: "Kamu yakin ingin keluar dari permainan ini? ",
  promptExitLevel: "Kamu yakin ingin keluar dari level ini? ",
  promptFinish: "Kuis Selesai",
  promptFinishText: "Apakah Kamu sudah yakin dengan jawabanmu?",
  promptFinishText2: "Apakah Kamu sudah yakin dengan jawabanmu? Kamu belum memeriksa jawaban no: {number}",
  promptFinishNext: "Ya, Lanjutkan",
  scoreText: "Selamat! Kamu telah menyelesaikan semua soal pada level ini!",
  video: "Video",
  article: "Teks",
  materi1: "MATERI 1 : <br>TANDA BIRAMA & TANDA KUNCI",
  materi2: "MATERI 2 : <br>HARGA NADA & NAMA NADA",
  materi3: "MATERI 3 : <br>TANGGA NADA",
  materi4: "MATERI 4 : <br>RITMIS",
  video1: "VIDEO 1 : <br>TANDA BIRAMA & TANDA KUNCI",
  video2: "VIDEO 2 : <br>HARGA NADA & NAMA NADA",
  video3: "VIDEO 3 : <br>TANGGA NADA",
  video4: "VIDEO 4 : <br>RITMIS",
  hide: "Sembunyikan",
  popupTitle: "Terjadi Kesalahan",
  popupMessage: "Silahkan coba beberapa saat lagi",
  succesSendData: "Data Berhasil Dikirim!",
  succesSendDataText:
    "Terimakasih atas partisipasinya, data yang dikumpulkan akan digunakan untuk keperluan penelitian",
  failedSendData: "Terimakasih!",
  failedSendDataText: "Kamu sudah berhasil menyelesaikan tahap ini",
  aboutGame: "TENTANG GAME",
  profile: "PROFILE",
  description: "DESKRIPSI GAME",
  goal: "TUJUAN PEMBELAJARAN",
  profileText: `<table>
  <tr>
    <td>NAMA PENGEMBANG</td>
    <td> : </td>
    <td>ASHILFASYA RAHMADHANI</td>
  </tr>
  <tr>
    <td>PROGRAM STUDI</td>
    <td> : </td>
    <td>PENDIDIKAN MUSIK</td>
  </tr>
  <tr>
    <td>NIM</td>
    <td> : </td>
    <td>18208244020</td>
  </tr>
   <tr>
    <td>INSTANSI</td>
    <td> : </td>
    <td>UNIVERSITAS NEGERI YOGYAKARTA</td>
  </tr>
   <tr>
    <td>EMAIL</td>
    <td> : </td>
    <td>ASHILFASYAR@GMAIL.COM</td>
  </tr>
</table>`,
  descriptionText:
    "GAME EDUKASI INI BERISI TENTANG MATERI PEMBELAJARAN NOTASI BALOK. MATERI TERSEBUT MELIPUTI TANDA BIRAMA, TANDA KUNCI, HARGA NADA, NAMA NADA, TANGGA NADA, DAN RITMIS. MATERI DISESUAIKAN DENGAN PEMBELAJARAN SENI MUSIK SISWA KELAS IX TINGKAT SMP.",
  goalText: "DAPAT MENINGKATKAN KEMAMPUAN MEMBACA NOTASI BALOK DAN MENINGKATKAN MINAT UNTUK BELAJAR SENI MUSIK.",
  materiType: "MATERI",
  materiList: "DAFTAR MATERI",
  videoList: "DAFTAR VIDEO",
  gameList: "DAFTAR PERMAINAN",
  summary: "Hasil Pertandingan",
  difficult: "Tingkat Kesulitan",
  topic: "Topik",
  score: "Nilai",
  levelCompleted: "Permainan Selesai",
  levelCompletedtext: "Selamat Kamu sudah menyelesaikan semua level!",
  noData: "Belum ada data",
  number: "Nomor",
  date: "Tanggal",
  titlelevel1: "Level 1 : Tanda Birama & Tanda Kunci",
  titlelevel2: "Level 2 : Harga Nada & Nama Nada",
  titlelevel3: "Level 3 : Tangga Nada",
  titlelevel4: "Level 4 : Ritmis",
  gameHint: "Berfungsi untuk menuju ke halaman main dan digunakan untuk mulai mengerjakan soal dari level 1 – 4 ",
  hintHint: "Berfungsi untuk menuju ke halaman petunjuk",
  historyHint: "Berfungsi untuk menuju ke halaman riwayat skor yang telah diperoleh",
  soundHint: "Berfungsi untuk mengatur suara",
  materiHint: "Berfungsi untuk menuju ke halaman materi",
  aboutHint: "Berfungsi untuk menuju ke halaman tentang game (profil, deskripsi, dan tujuan)",
  surveyHint: "Berfungsi untuk mengisi angket responden penelitian",
  exitHint: "Berfungsi untuk keluar dari game",
  loadSound: "sedang menyiapkan suara, mohon tunggu sebentar..",
  loadVideo: "sedang menyiapkan video, mohon tunggu sebentar..",
  hintGameTitle: "Petunjuk Game",
  hintGame: `<ol>
	<li>Permainan terdiri dari 4 level, masing-masing level memiliki 10 soal yang perlu dijawab oleh pemain.</li>
	<li>Sebelum memulai game, pemain boleh membaca materi yang sudah disiapkan dalam menu materi, ataupun langsung mengerjakan soal.</li>
	<li>Untuk membuka level yang masih terkunci, pemain harus menyelesaikan level sebelumnya.</li>
	<li>Setelah memilih jawaban, jika pemain sudah yakin dengan jawabannya, pemain dapat menekan tombol "Periksa Jawaban".</li>
	<li>Soal yang sudah diperiksa akan di kunci dan akan menampilkan jawaban yang benar.</li>
	<li>Soal yang sudah diperiksa tidak bisa diganti lagi</li>
	<li>Jika pemain sudah mengisi semua soal dan sudah yakin dengan jawabannya, pemain dapat menekan tombol selesai.</li>
	<li>Setelah pemain menekam tombol selesai, pemain dapat melihat score yang didapat, serta dapat melihat kunci jawaban dan pembahasan dari kuis tersebut.</li>
</ol>`,
};

export const url = {
  survey: "https://forms.gle/Fus5Y3ZgoYVuEeLo6",
  guitar: "https://video-public.canva.com/VAECoeug-Ho/v/99454e9883.gif",
  kecrek: "https://video-public.canva.com/VAECoXaNOWw/v/84f6669a75.gif",
  mainIcon: "https://media-public.canva.com/GqMco/MAD0tTGqMco/1/t.png",
  exit: "https://media-public.canva.com/80KFY/MAEFhf80KFY/1/t.png",
  game: "https://media-public.canva.com/ZOeUs/MAEmH5ZOeUs/1/t.png",
  materi: "https://media-public.canva.com/pLnFA/MADiEDpLnFA/2/t.png",
  hint: "https://media-public.canva.com/IevkU/MADrPCIevkU/2/t.png",
  about: "https://media-public.canva.com/ifba4/MAELqpifba4/1/t.png",
  history: "https://media-public.canva.com/hY79w/MAFDd3hY79w/1/t.png",
  surveyIcon: "https://media-public.canva.com/MADoOkaeBVk/1/thumbnail.png",
  video1: "https://youtu.be/OSFH2l4QLoM",
  video2: "https://youtu.be/Gwgthvf7dOI",
  video3: "https://youtu.be/2cX_mZkQLZw",
  video4: "https://youtu.be/dURNMp8L4V4",
};
