import React from "react";

import classes from "./style.module.scss";

const LightLayout = ({ children }) => {
  return <div className={classes.background}>{children}</div>;
};

export default LightLayout;
