import React from "react";

import classes from "./style.module.scss";

const DarkLayout = ({ children }) => {
  return <div className={classes.background}>{children}</div>;
};

export default DarkLayout;
