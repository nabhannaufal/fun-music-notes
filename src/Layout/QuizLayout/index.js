import React from "react";

import classes from "./style.module.scss";

const QuizLayout = ({ children }) => {
  return <div className={classes.background}>{children}</div>;
};

export default QuizLayout;
