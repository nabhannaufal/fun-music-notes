import { takeLatest, put, call } from "redux-saga/effects";
import { SEND_DATA } from "./constants";
import { setLoading, openPopup } from "./actions";
import { postSheet } from "Domain/api";

export function* doSendData({ row, level, cbSuccess }) {
  yield put(setLoading(true));
  try {
    const response = yield call(postSheet, row, level);
    if (response) {
      cbSuccess && cbSuccess();
    }
  } catch (error) {
    console.log(error);
    yield put(openPopup(true));
  }
  yield put(setLoading(false));
}

export default function* appSaga() {
  yield takeLatest(SEND_DATA, doSendData);
}
