import { createSelector } from "reselect";
import { initialState } from "./reducer";

const selectStarterpackState = (state) => state.app || initialState;

const selectOpenPopup = createSelector(selectStarterpackState, (state) => state.openPopup);
const selectPopupTitle = createSelector(selectStarterpackState, (state) => state.popupTitle);
const selectPopupMessage = createSelector(selectStarterpackState, (state) => state.popupMessage);
const selectPopupError = createSelector(selectStarterpackState, (state) => state.popupError);
const selectLoading = createSelector(selectStarterpackState, (state) => state.loading);
const selectPlayBacksound = createSelector(selectStarterpackState, (state) => state.playBacksound);
const selectMuteBacksound = createSelector(selectStarterpackState, (state) => state.muteBacksound);
const selectPromptExit = createSelector(selectStarterpackState, (state) => state.promptExit);
const selectOpenHint = createSelector(selectStarterpackState, (state) => state.openHint);

export {
  selectOpenPopup,
  selectPopupTitle,
  selectPopupMessage,
  selectPopupError,
  selectLoading,
  selectPlayBacksound,
  selectMuteBacksound,
  selectPromptExit,
  selectOpenHint,
};
