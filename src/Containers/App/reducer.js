import produce from "immer";
import { wording } from "Wording";

import {
  SET_OPEN_POPUP,
  SET_LOADING,
  SET_PLAY_BACKSOUND,
  SET_MUTE_BACKSOUND,
  SET_PROMPT_EXIT,
  SET_OPEN_HINT,
} from "./constants";

export const initialState = {
  openPopup: false,
  popupTitle: wording.popupTitle,
  popupMessage: wording.popupMessage,
  popupError: true,
  loading: false,
  playBacksound: false,
  muteBacksound: false,
  promptExit: false,
  openHint: false,
};

/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_OPEN_POPUP:
        draft.openPopup = action.openPopup;
        break;
      case SET_LOADING:
        draft.loading = action.loading;
        break;
      case SET_PLAY_BACKSOUND:
        draft.playBacksound = action.playBacksound;
        break;
      case SET_MUTE_BACKSOUND:
        draft.muteBacksound = action.muteBacksound;
        break;
      case SET_PROMPT_EXIT:
        draft.promptExit = action.promptExit;
        break;
      case SET_OPEN_HINT:
        draft.openHint = action.openHint;
        break;
    }
  });

export default appReducer;
