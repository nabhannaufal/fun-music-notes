import React from "react";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";
import { closePopup, setPromptExit } from "./actions";
import {
  selectOpenPopup,
  selectLoading,
  selectPlayBacksound,
  selectMuteBacksound,
  selectPromptExit,
} from "./selectors";

import ClientRoutes from "Routes";
import PopupDialog from "Components/PopupDialog";
import PopupExit from "Components/PopupExit";
import Loader from "Components/Loader";
import SoundControl from "Components/SoundControl";

import "moment/locale/id";
import "Style/global.scss";
import { wording } from "Wording";

const App = ({ openPopup, loading, playBacksound, muteBacksound, promptExit }) => {
  const dispatch = useDispatch();
  const onClose = () => {
    dispatch(closePopup());
  };
  const closePromptExit = () => {
    dispatch(setPromptExit(false));
  };
  return (
    <>
      <Loader isLoading={loading} />
      <ClientRoutes />
      <SoundControl isPlay={playBacksound && !muteBacksound} />
      <PopupDialog open={openPopup} title={wording.popupTitle} message={wording.popupMessage} onClose={onClose} />
      <PopupExit
        open={promptExit}
        onClose={closePromptExit}
        title={wording.promptExitLevel}
        onClickYes={() => {
          window.location.replace("/game");
        }}
        onClickNo={closePromptExit}
      />
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  openPopup: selectOpenPopup,
  loading: selectLoading,
  playBacksound: selectPlayBacksound,
  muteBacksound: selectMuteBacksound,
  promptExit: selectPromptExit,
});

App.propTypes = {
  openPopup: PropTypes.bool,
  laoding: PropTypes.bool,
  playBacksound: PropTypes.bool,
  muteBacksound: PropTypes.bool,
  promptExit: PropTypes.bool,
};

export default connect(mapStateToProps)(App);
