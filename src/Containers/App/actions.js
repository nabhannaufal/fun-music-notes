import {
  SET_OPEN_POPUP,
  SET_LOADING,
  SEND_DATA,
  SET_PLAY_BACKSOUND,
  SET_MUTE_BACKSOUND,
  SET_PROMPT_EXIT,
  SET_OPEN_HINT,
} from "./constants";

export function openPopup() {
  return {
    type: SET_OPEN_POPUP,
    openPopup: true,
  };
}

export function closePopup() {
  return {
    type: SET_OPEN_POPUP,
    openPopup: false,
  };
}

export function setLoading(loading) {
  return {
    type: SET_LOADING,
    loading,
  };
}

export function sendData(row, level, cbSuccess) {
  return {
    type: SEND_DATA,
    row,
    level,
    cbSuccess,
  };
}

export function setPlayBacksound(playBacksound) {
  return {
    type: SET_PLAY_BACKSOUND,
    playBacksound,
  };
}

export function setMuteBacksound(muteBacksound) {
  return {
    type: SET_MUTE_BACKSOUND,
    muteBacksound,
  };
}

export function setPromptExit(promptExit) {
  return {
    type: SET_PROMPT_EXIT,
    promptExit,
  };
}

export function setOpenHint(openHint) {
  return {
    type: SET_OPEN_HINT,
    openHint,
  };
}
