import { combineReducers } from "redux";

import appReducer from "Containers/App/reducer";
import funNoteReducer, { storedKey as storedFunNote } from "Pages/reducer";
import { mapWithPersistor } from "./persistence";

// * reducers that will stored to localStorage
const storedReducers = {
  funNoteState: { reducer: funNoteReducer, whitelist: storedFunNote },
};

const temporaryReducers = {
  app: appReducer,
};

export default function createRecuer() {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });

  /* eslint-disable no-param-reassign */
  const rootReducer = (state, action) => {
    return coreReducer(state, action);
  };

  return rootReducer;
}
