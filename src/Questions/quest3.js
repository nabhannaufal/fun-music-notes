export const quest3 = [
  {
    id: 1,
    title: "Soal 1",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. C Mayor",
      },
      {
        value: "B",
        detail: "b.	A minor",
      },
      {
        value: "C",
        detail: "c.	G Mayor",
      },
      {
        value: "D",
        detail: "d.	E minor",
      },
    ],
    imgUrl: "/soal/level3_1.png",
    correctAnswer: "B",
    pembahasan:
      "<strong>Jawaban benar: b. A minor</strong><br><br>Tangga nada A minor merupakan tangga nada yang seluruh nadanya belum mengalami perubahan dan dinamakan tangga nada minor natural. Susunan nada dalam tangga nada a minor adalah a - b - c - d - e - f - g - a.",
  },
  {
    id: 2,
    title: "Soal 2",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. C Mayor",
      },
      {
        value: "B",
        detail: "b.	A minor",
      },
      {
        value: "C",
        detail: "c.	G Mayor",
      },
      {
        value: "D",
        detail: "d.	E minor",
      },
    ],
    imgUrl: "/soal/level3_2.png",
    correctAnswer: "A",
    pembahasan:
      "<strong>Jawaban benar: a. C Mayor</strong><br><br>Tangga nada A minor merupakan tangga nada yang seluruh nadanya belum mengalami perubahan dan dinamakan tangga nada minor natural. Susunan nada dalam tangga nada a minor adalah a - b - c - d - e - f - g - a.",
  },
  {
    id: 3,
    title: "Soal 3",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. B Mayor",
      },
      {
        value: "B",
        detail: "b.	G Mayor",
      },
      {
        value: "C",
        detail: "c.	D Mayor",
      },
      {
        value: "D",
        detail: "d.	E Mayor",
      },
    ],
    imgUrl: "/soal/level3_3.png",
    correctAnswer: "C",
    pembahasan:
      "<strong>Jawaban benar: c. D Mayor</strong><br><br>Dalam tangga nada D mayor, susunan nada yang mendapat perubahan atau dinaikkan 1 semitone (setengah) adalah nada ke-3 (tiga) dan nada ke-7 (tujuh). Sehingga tangga nada D mayor tersusun menjadi D - E - Fis - G - A - B - Cis - D.",
  },
  {
    id: 4,
    title: "Soal 4",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. B Mayor",
      },
      {
        value: "B",
        detail: "b.	G Mayor",
      },
      {
        value: "C",
        detail: "c.	D Mayor",
      },
      {
        value: "D",
        detail: "d.	E Mayor",
      },
    ],
    imgUrl: "/soal/level3_4.png",
    correctAnswer: "B",
    pembahasan:
      "<strong>Jawaban benar: b. G Mayor</strong><br><br>Dalam tangga nada G mayor, susunan nada ke-7 (tujuh) mengalami perubahan, yaitu dinaikkan 1 semitone (setengah), karena dalam nada ke 7-1 harus memiliki jarak 1/2 (setengah) atau 1 semitone. Sehingga tangga nada G mayor tersusun menjadi G - A - B - C - D - E - Fis - G.",
  },
  {
    id: 5,
    title: "Soal 5",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. B Mayor",
      },
      {
        value: "B",
        detail: "b.	G Mayor",
      },
      {
        value: "C",
        detail: "c.	D Mayor",
      },
      {
        value: "D",
        detail: "d.	F Mayor",
      },
    ],
    imgUrl: "/soal/level3_5.png",
    correctAnswer: "D",
    pembahasan:
      "<strong>Jawaban benar: d. F Mayor</strong><br><br>Dalam tangga nada F mayor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) nada adalah nada ke-4 (empat). Sehingga tangga nada F mayor tersusun menjadi F - G - A - Bes - C - D - E - F.",
  },
  {
    id: 6,
    title: "Soal 6",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. E minor",
      },
      {
        value: "B",
        detail: "b.	A minor",
      },
      {
        value: "C",
        detail: "c.	C Mayor",
      },
      {
        value: "D",
        detail: "d.	D Mayor",
      },
    ],
    imgUrl: "/soal/level3_6.png",
    correctAnswer: "A",
    pembahasan:
      "<strong>Jawaban benar: a. E minor</strong><br><br>Dalam tangga nada e minor, susunan nada ke-2 mengalami perubahan, yaitu dinaikkan 1 semitone (setengah), karena dalam nada ke 2-3 harus memiliki jarak 1/2 (setengah) atau 1 semitone. Sehingga tangga nada e minor tersusun menjadi e - fis - g - a - b - c - d - e.",
  },
  {
    id: 7,
    title: "Soal 7",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. Cis Mayor",
      },
      {
        value: "B",
        detail: "b.	B minor",
      },
      {
        value: "C",
        detail: "c.	G Mayor",
      },
      {
        value: "D",
        detail: "d.	E minor",
      },
    ],
    imgUrl: "/soal/level3_7.png",
    correctAnswer: "B",
    pembahasan:
      "<strong>Jawaban benar: b. B minor</strong><br><br>Dalam tangga nada b minor, susunan nada yang mendapat perubahan atau dinaikkan 1 semitone (setengah) adalah nada ke-2 (dua) dan nada ke-5 (lima). Sehingga tangga nada b minor tersusun menjadi b - cis - d - e - fis - g - a - b.",
  },
  {
    id: 8,
    title: "Soal 8",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. G Mayor",
      },
      {
        value: "B",
        detail: "b. B Mayor",
      },
      {
        value: "C",
        detail: "c.	Bes Mayor",
      },
      {
        value: "D",
        detail: "d.	E Mayor",
      },
    ],
    imgUrl: "/soal/level3_8.png",
    correctAnswer: "C",
    pembahasan:
      "<strong>Jawaban benar: c. Bes Mayor</strong><br><br>Dalam tangga nada Bes mayor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) nada adalah nada ke-1 (satu) dan nada ke-4 (empat). Sehingga tangga nada Bes mayor tersusun menjadi Bes - C - D - Es - F - G - A - Bes.",
  },
  {
    id: 9,
    title: "Soal 9",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. G minor",
      },
      {
        value: "B",
        detail: "b.	A minor",
      },
      {
        value: "C",
        detail: "c.	Cis Mayor",
      },
      {
        value: "D",
        detail: "d.	Fis minor",
      },
    ],
    imgUrl: "/soal/level3_9.png",
    correctAnswer: "A",
    pembahasan:
      "<strong>Jawaban benar: a. G minor</strong><br><br>Dalam tangga nada g minor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) adalah nada ke-3 (tiga) dan nada ke -6 (enam). Sehingga tangga nada g minor tersusun menjadi g - a - bes - c - d - es - f - g.",
  },
  {
    id: 10,
    title: "Soal 10",
    description: "Tangga nada dibawah ini merupakan tangga nada …",
    choiche: [
      {
        value: "A",
        detail: "a. Es Mayor",
      },
      {
        value: "B",
        detail: "b.	B minor",
      },
      {
        value: "C",
        detail: "c.	D Mayor",
      },
      {
        value: "D",
        detail: "d.	E Mayor",
      },
    ],
    imgUrl: "/soal/level3_10.png",
    correctAnswer: "A",
    pembahasan:
      "<strong>Jawaban benar: a. Es Mayor</strong><br><br>Dalam tangga nada Es mayor, susunan nada yang mendapat perubahan atau diturunkan 1 semitone (setengah) nada adalah nada ke-1 (satu), nada ke-4 (empat), dan nada ke-5 (lima). Sehingga tangga nada Es mayor tersusun menjadi Es - F - G - As - Bes - C - D - Es.",
  },
];
