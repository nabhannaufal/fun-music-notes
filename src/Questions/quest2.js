export const quest2 = [
  {
    id: 1,
    title: "Soal 1",
    description: "Harga nada dalam not berikut memiliki nilai …",
    choiche: [
      {
        value: "A",
        detail: "a. 1 ketuk",
      },
      {
        value: "B",
        detail: "b.	2 ketuk",
      },
      {
        value: "C",
        detail: "c.	3 ketuk",
      },
      {
        value: "D",
        detail: "d.	4 ketuk",
      },
    ],
    imgUrl2: "/soal/level2_1.png",
    correctAnswer: "B",
    pembahasan: `<strong>Jawaban benar: b. 2 ketuk</strong><br><br>tanda berikut merupakan not setengah (1/2) yang memiliki nilai 2 ketuk, artinya dalam setiap nada yang dituliskan dengan tanda tersebut dimainkan selama 2 ketukan.`,
  },
  {
    id: 2,
    title: "Soal 2",
    description: "Harga nada dalam not berikut memiliki nilai …",
    choiche: [
      {
        value: "A",
        detail: "a. 1 ketuk",
      },
      {
        value: "B",
        detail: "b.	2 ketuk",
      },
      {
        value: "C",
        detail: "c.	3 ketuk",
      },
      {
        value: "D",
        detail: "d.	4 ketuk",
      },
    ],
    imgUrl2: "/soal/level2_2.png",
    correctAnswer: "D",
    pembahasan: `<strong>Jawaban benar: d. 4 ketuk</strong><br><br>tanda berikut merupakan not utuh (penuh) yang memiliki nilai 4 ketuk, artinya dalam setiap nada yang dituliskan dengan tanda tersebut dimainkan selama 4 ketukan.`,
  },
  {
    id: 3,
    title: "Soal 3",
    description: "Harga nada dalam not berikut memiliki nilai …",
    choiche: [
      {
        value: "A",
        detail: "a. 1 ketuk",
      },
      {
        value: "B",
        detail: "b.	2 ketuk",
      },
      {
        value: "C",
        detail: "c.	3 ketuk",
      },
      {
        value: "D",
        detail: "d.	4 ketuk",
      },
    ],
    imgUrl2: "/soal/level2_3.png",
    correctAnswer: "A",
    pembahasan: `<strong>Jawaban benar: a. 1 ketuk</strong><br><br>tanda berikut merupakan not seperempat (1/4) yang memiliki nilai 1 ketuk, artinya dalam setiap nada yang dituliskan dengan tanda tersebut dimainkan selama 1 ketukan.`,
  },
  {
    id: 4,
    title: "Soal 4",
    description: "Harga nada dalam not berikut memiliki nilai …",
    choiche: [
      {
        value: "A",
        detail: "a. 1 ketuk",
      },
      {
        value: "B",
        detail: "b.	1/2 ketuk",
      },
      {
        value: "C",
        detail: "c.	1/4 ketuk",
      },
      {
        value: "D",
        detail: "d.	1/8 ketuk",
      },
    ],
    imgUrl2: "/soal/level2_4.png",
    correctAnswer: "B",
    pembahasan: `<strong>Jawaban benar: b. 1/2 ketuk</strong><br><br>tanda berikut merupakan not seperdelapan (1/8) yang memiliki nilai 1/2 ketuk, artinya dalam setiap nada yang dituliskan dengan tanda tersebut dimainkan selama 1/2 ketukan.`,
  },
  {
    id: 5,
    title: "Soal 5",
    description: "Not seperempat (1/4) memiliki bentuk …",
    choiche: [
      {
        value: "A",
        detail: "a.",
        detailImg: "/soal/level2_5a.png",
      },
      {
        value: "B",
        detail: "b.",
        detailImg: "/soal/level2_5b.png",
      },
      {
        value: "C",
        detail: "c.",
        detailImg: "/soal/level2_5c.png",
      },
      {
        value: "D",
        detail: "d.",
        detailImg: "/soal/level2_5d.png",
      },
    ],
    correctAnswer: "C",
    pembahasan: `<strong>Jawaban benar: c. </strong><br><br>Not tersebut merupakan not seperempat (1/4) dan bernilai 1 ketukan.`,
  },
  {
    id: 6,
    title: "Soal 6",
    description: "Not seperenambelas (1/16) memiliki bentuk …",
    choiche: [
      {
        value: "A",
        detail: "a.",
        detailImg: "/soal/level2_6a.png",
      },
      {
        value: "B",
        detail: "b.",
        detailImg: "/soal/level2_6b.png",
      },
      {
        value: "C",
        detail: "c.",
        detailImg: "/soal/level2_6c.png",
      },
      {
        value: "D",
        detail: "d.",
        detailImg: "/soal/level2_6d.png",
      },
    ],
    correctAnswer: "A",
    pembahasan: `<strong>Jawaban benar: a. </strong><br><br>Not tersebut merupakan not seperenambelas (1/16) dan bernilai 1/4 ketukan.`,
  },
  {
    id: 7,
    title: "Soal 7",
    description: "Tanda istirahat dalam not seperempat (1/4) adalah  …",
    choiche: [
      {
        value: "A",
        detail: "a.",
        detailImg: "/soal/level2_7a.png",
      },
      {
        value: "B",
        detail: "b.",
        detailImg: "/soal/level2_7b.png",
      },
      {
        value: "C",
        detail: "c.",
        detailImg: "/soal/level2_7c.png",
      },
      {
        value: "D",
        detail: "d.",
        detailImg: "/soal/level2_7d.png",
      },
    ],
    correctAnswer: "C",
    pembahasan: `<strong>Jawaban benar: c. </strong><br><br>Tanda tersebut merupakan tanda istirahat dalam not seperempat (1/4) yang bernilai istirahat 1 ketukan.`,
  },
  {
    id: 8,
    title: "Soal 8",
    description: "Nada dibawah ini memiliki nama nada …",
    choiche: [
      {
        value: "A",
        detail: "a.	G",
      },
      {
        value: "B",
        detail: "b.	B",
      },
      {
        value: "C",
        detail: "c.	E",
      },
      {
        value: "D",
        detail: "d.	D",
      },
    ],
    imgUrl: "/soal/level2_8.png",
    correctAnswer: "A",
    pembahasan: `<strong>Jawaban benar: a. G </strong><br><br>Nada tersebut memiliki nama nada G dan dituliskan dengan menggunakan kunci G.`,
  },
  {
    id: 9,
    title: "Soal 9",
    description: "Nada dibawah ini memiliki nama nada …",
    choiche: [
      {
        value: "A",
        detail: "a.	G",
      },
      {
        value: "B",
        detail: "b.	B",
      },
      {
        value: "C",
        detail: "c.	C",
      },
      {
        value: "D",
        detail: "d.	D",
      },
    ],
    imgUrl: "/soal/level2_9.png",
    correctAnswer: "C",
    pembahasan: `<strong>Jawaban benar: c. C </strong><br<br>Nada tersebut memiliki nama nada C dan dituliskan dengan menggunakan kunci C.`,
  },
  {
    id: 10,
    title: "Soal 10",
    description: "Nada dibawah ini memiliki nama nada …",
    choiche: [
      {
        value: "A",
        detail: "a.	B",
      },
      {
        value: "B",
        detail: "b.	D",
      },
      {
        value: "C",
        detail: "c.	F",
      },
      {
        value: "D",
        detail: "d.	A",
      },
    ],
    imgUrl: "/soal/level2_10.png",
    correctAnswer: "C",
    pembahasan: `<strong>Jawaban benar: c. F </strong><br><br>Nada tersebut memiliki nama nada F dan dituliskan dengan menggunakan kunci F.`,
  },
];
