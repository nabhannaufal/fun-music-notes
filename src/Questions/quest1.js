export const quest1 = [
  {
    id: 1,
    title: "Soal 1",
    description: "Tanda yang berfungsi untuk menunjukkan satuan ketukan dan jumlah ketukan dalam birama disebut …",
    choiche: [
      {
        value: "A",
        detail: "a. Tanda kunci",
      },
      {
        value: "B",
        detail: "b.	Garis paranada",
      },
      {
        value: "C",
        detail: "c.	Tanda birama",
      },
      {
        value: "D",
        detail: "d.	Notasi balok",
      },
    ],
    correctAnswer: "C",
    pembahasan:
      "<strong>Jawaban benar: c</strong><br><br>Tanda birama adalah tanda dalam notasi balok yang terletak pada awal suatu bagian dalam musik. Tanda ini berfungsi untuk menunjukkan satuan ketukan dan jumlah ketukan dalam birama",
  },
  {
    id: 2,
    title: "Soal 2",
    description: "Berapakah tanda birama dalam pola ritme dibawah ini",
    choiche: [
      {
        value: "A",
        detail: "a.	3/2",
      },
      {
        value: "B",
        detail: "b.	3/4",
      },
      {
        value: "C",
        detail: "c.	4/4",
      },
      {
        value: "D",
        detail: "d.	2/2",
      },
    ],
    imgUrl: "/soal/level1_2.png",
    correctAnswer: "B",
    pembahasan:
      "<strong>Jawaban benar: b</strong><br><br>Tanda birama dalam pola ritme tersebut adalah 3/4, karena dalam satu birama terdapat 3 ketukan dengan harga setiap not bernilai not 1/4.",
  },
  {
    id: 3,
    title: "Soal 3",
    description: "Berapakah tanda birama dalam pola ritme dibawah ini?",
    choiche: [
      {
        value: "A",
        detail: "a.	3/2",
      },
      {
        value: "B",
        detail: "b.	4/4",
      },
      {
        value: "C",
        detail: "c.	3/4",
      },
      {
        value: "D",
        detail: "d.	2/2",
      },
    ],
    imgUrl: "/soal/level1_3.png",
    correctAnswer: "B",
    pembahasan:
      "<strong>Jawaban benar: b</strong><br><br>Tanda birama dalam pola ritme tersebut adalah 4/4, karena dalam satu birama terdapat 4 ketukan dengan harga setiap not bernilai not 1/4.",
  },
  {
    id: 4,
    title: "Soal 4",
    description: "Berapakah tanda birama dalam pola ritme dibawah ini?",
    choiche: [
      {
        value: "A",
        detail: "a.	3/2",
      },
      {
        value: "B",
        detail: "b.	3/4",
      },
      {
        value: "C",
        detail: "c.	2/4",
      },
      {
        value: "D",
        detail: "d.	2/2",
      },
    ],
    imgUrl: "/soal/level1_4.png",
    correctAnswer: "A",
    pembahasan:
      "<strong>Jawaban benar: a</strong><br><br>Tanda birama dalam pola ritme tersebut adalah 3/2, karena dalam satu birama terdapat 3 ketukan dengan harga setiap not bernilai not 1/2.",
  },
  {
    id: 5,
    title: "Soal 5",
    description: "Berapakah tanda birama dalam pola ritme dibawah ini?",
    choiche: [
      {
        value: "A",
        detail: "a.	12/8",
      },
      {
        value: "B",
        detail: "b.	9/8",
      },
      {
        value: "C",
        detail: "c.	6/8",
      },
      {
        value: "D",
        detail: "d.	3/8",
      },
    ],
    imgUrl: "/soal/level1_5.png",
    correctAnswer: "C",
    pembahasan:
      "<strong>Jawaban benar: c</strong><br><br>Tanda birama dalam pola ritme tersebut adalah 6/8, karena dalam satu birama terdapat 6 ketukan dengan harga setiap not bernilai not 1/8.",
  },
  {
    id: 6,
    title: "Soal 6",
    description: "Berapakah tanda birama dalam pola ritme dibawah ini?",
    choiche: [
      {
        value: "A",
        detail: "a.	3/8",
      },
      {
        value: "B",
        detail: "b.	6/8",
      },
      {
        value: "C",
        detail: "c.	9/8",
      },
      {
        value: "D",
        detail: "d.	12/8",
      },
    ],
    imgUrl: "/soal/level1_6.png",
    correctAnswer: "D",
    pembahasan:
      "<strong>Jawaban benar: d</strong><br><br>Tanda birama dalam pola ritme tersebut adalah 12/8, karena dalam satu birama terdapat 12 ketukan dengan harga setiap not bernilai not 1/8.",
  },
  {
    id: 7,
    title: "Soal 7",
    description: "Berapakah tanda birama dalam pola ritme dibawah ini?",
    choiche: [
      {
        value: "A",
        detail: "a.	3/4",
      },
      {
        value: "B",
        detail: "b.	3/8",
      },
      {
        value: "C",
        detail: "c.	6/8",
      },
      {
        value: "D",
        detail: "d.	9/8",
      },
    ],
    imgUrl: "/soal/level1_7.png",
    correctAnswer: "D",
    pembahasan:
      "<strong>Jawaban benar: d</strong><br><br>Tanda birama dalam pola ritme tersebut adalah 9/8, karena dalam satu birama terdapat 9 ketukan dengan harga setiap not bernilai not 1/8.",
  },
  {
    id: 8,
    title: "Soal 8",
    description: "Gambar dibawah ini merupakan tanda kunci …",
    choiche: [
      {
        value: "A",
        detail: "a.	G",
      },
      {
        value: "B",
        detail: "b.	C",
      },
      {
        value: "C",
        detail: "c.	F",
      },
      {
        value: "D",
        detail: "d.	D",
      },
    ],
    imgUrl: "/soal/level1_8.png",
    correctAnswer: "A",
    pembahasan: "<strong>Jawaban benar: a</strong><br><br>Tanda kunci dalam gambar tersebut adalah kunci G.",
  },
  {
    id: 9,
    title: "Soal 9",
    description: "Gambar dibawah ini merupakan tanda kunci …",
    choiche: [
      {
        value: "A",
        detail: "a.	G",
      },
      {
        value: "B",
        detail: "b.	C",
      },
      {
        value: "C",
        detail: "c.	F",
      },
      {
        value: "D",
        detail: "d.	D",
      },
    ],
    imgUrl: "/soal/level1_9.png",
    correctAnswer: "C",
    pembahasan: "<strong>Jawaban benar: c</strong><br><br>Tanda kunci dalam gambar tersebut adalah kunci F.",
  },
  {
    id: 10,
    title: "Soal 10",
    description: "Gambar dibawah ini merupakan tanda kunci …",
    choiche: [
      {
        value: "A",
        detail: "a.	G",
      },
      {
        value: "B",
        detail: "b.	C",
      },
      {
        value: "C",
        detail: "c.	F",
      },
      {
        value: "D",
        detail: "d.	D",
      },
    ],
    imgUrl: "/soal/level1_10.png",
    correctAnswer: "B",
    pembahasan: "<strong>Jawaban benar: b</strong><br><br>Tanda kunci dalam gambar tersebut adalah kunci C.",
  },
];
