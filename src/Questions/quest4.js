export const quest4 = [
  {
    id: 1,
    title: "Soal 1",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 0 },
      { option: "B", id: 1 },
      { option: "C", id: 2 },
      { option: "D", id: 3 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_1.png",
    correctAnswer: "A",
    pembahasan: "<strong>Jawaban benar: a</strong>",
  },
  {
    id: 2,
    title: "Soal 2",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 8 },
      { option: "B", id: 1 },
      { option: "C", id: 0 },
      { option: "D", id: 3 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_2.png",
    correctAnswer: "B",
    pembahasan: "<strong>Jawaban benar: b</strong>",
  },
  {
    id: 3,
    title: "Soal 3",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 3 },
      { option: "B", id: 1 },
      { option: "C", id: 2 },
      { option: "D", id: 6 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_3.png",
    correctAnswer: "C",
    pembahasan: "<strong>Jawaban benar: c</strong>",
  },
  {
    id: 4,
    title: "Soal 4",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 6 },
      { option: "B", id: 5 },
      { option: "C", id: 4 },
      { option: "D", id: 3 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_4.png",
    correctAnswer: "D",
    pembahasan: "<strong>Jawaban benar: d</strong>",
  },
  {
    id: 5,
    title: "Soal 5",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 3 },
      { option: "B", id: 9 },
      { option: "C", id: 7 },
      { option: "D", id: 4 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_5.png",
    correctAnswer: "D",
    pembahasan: "<strong>Jawaban benar: d</strong>",
  },
  {
    id: 6,
    title: "Soal 6",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 5 },
      { option: "B", id: 6 },
      { option: "C", id: 7 },
      { option: "D", id: 8 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_6.png",
    correctAnswer: "A",
    pembahasan: "<strong>Jawaban benar: a</strong>",
  },
  {
    id: 7,
    title: "Soal 7",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 2 },
      { option: "B", id: 6 },
      { option: "C", id: 8 },
      { option: "D", id: 0 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_7.png",
    correctAnswer: "B",
    pembahasan: "<strong>Jawaban benar: b</strong>",
  },
  {
    id: 8,
    title: "Soal 8",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 3 },
      { option: "B", id: 5 },
      { option: "C", id: 7 },
      { option: "D", id: 9 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_8.png",
    correctAnswer: "C",
    pembahasan: "<strong>Jawaban benar: c</strong>",
  },
  {
    id: 9,
    title: "Soal 9",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 0 },
      { option: "B", id: 8 },
      { option: "C", id: 1 },
      { option: "D", id: 9 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_9.png",
    correctAnswer: "B",
    pembahasan: "<strong>Jawaban benar: b</strong>",
  },
  {
    id: 10,
    title: "Soal 10",
    description: "Suara ritme yang sesuai dari pola ritme dibawah ini adalah …",
    descriptionSound: [
      { option: "A", id: 9 },
      { option: "B", id: 8 },
      { option: "C", id: 7 },
      { option: "D", id: 6 },
    ],
    choiche: [
      {
        value: "A",
        detail: "a. A",
      },
      {
        value: "B",
        detail: "b. B",
      },
      {
        value: "C",
        detail: "c. C",
      },
      {
        value: "D",
        detail: "d. D",
      },
    ],
    imgUrl: "/soal/level4_10.png",
    correctAnswer: "A",
    pembahasan: "<strong>Jawaban benar: a</strong>",
  },
];
