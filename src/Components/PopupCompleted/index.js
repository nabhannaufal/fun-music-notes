import React from "react";
import PropTypes from "prop-types";
import { Dialog, Drawer, useMediaQuery } from "@mui/material";

import classes from "./style.module.scss";

const PopupCompleted = ({ open, title, message, btnPrimaryText, onClickBtnPrimary }) => {
  const mobileView = useMediaQuery("(max-width:992px)");
  const renderPopup = (element) => {
    let props = {
      open,
    };

    if (mobileView) {
      props = {
        anchor: "bottom",
        PaperProps: { style: { borderRadius: "16px 16px 0 0" } },
        ...props,
      };
      return <Drawer {...props}>{element}</Drawer>;
    }
    return (
      <Dialog {...props} PaperProps={{ style: { borderRadius: "16px" } }}>
        {element}
      </Dialog>
    );
  };

  const popupWrapper = (
    <div className={classes.popupWrapper}>
      <div className={classes.title}>{title}</div>
      <div className={classes.message} dangerouslySetInnerHTML={{ __html: message }} />
      <div className={classes.btnWrapper}>
        {btnPrimaryText && (
          <button type="button" className={classes.button} onClick={() => onClickBtnPrimary()}>
            {btnPrimaryText}
          </button>
        )}
      </div>
    </div>
  );
  return renderPopup(popupWrapper);
};

PopupCompleted.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  btnPrimaryText: PropTypes.string,
  onClickBtnPrimary: PropTypes.func,
};

export default PopupCompleted;
