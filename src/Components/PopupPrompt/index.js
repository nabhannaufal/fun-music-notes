import React from "react";
import PropTypes from "prop-types";
import { Dialog, Drawer, useMediaQuery } from "@mui/material";

import { wording } from "Wording";
import closeIcon from "Images/closeIcon.png";

import classes from "./style.module.scss";

const PopupPrompt = ({ open, onClose, title, onClickYes, onClickNo }) => {
  const mobileView = useMediaQuery("(max-width:992px)");
  const renderPopup = (element) => {
    let props = {
      open,
      onClose,
    };

    if (mobileView) {
      props = {
        anchor: "bottom",
        PaperProps: { style: { borderRadius: "16px 16px 0 0" } },
        ...props,
      };
      return <Drawer {...props}>{element}</Drawer>;
    }
    return (
      <Dialog {...props} PaperProps={{ style: { borderRadius: "16px" } }}>
        {element}
      </Dialog>
    );
  };

  const popupWrapper = (
    <div className={classes.popupWrapper}>
      <div className={classes.popupTab}>
        <div className={classes.closeIcon} onClick={onClose}>
          <img src={closeIcon} alt={wording.images} />
        </div>
      </div>
      <div className={classes.title}>{title}</div>
      <div className={classes.btnWrapper}>
        <button type="button" className={classes.button2} onClick={() => onClickNo()}>
          {wording.no}
        </button>
        <button type="button" className={classes.button} onClick={() => onClickYes()}>
          {wording.yes}
        </button>
      </div>
    </div>
  );
  return renderPopup(popupWrapper);
};

PopupPrompt.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  onClickYes: PropTypes.func,
  onClickYNo: PropTypes.func,
};

export default PopupPrompt;
