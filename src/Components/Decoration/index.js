import React from "react";
import PropTypes from "prop-types";

import useMediaQuery from "@mui/material/useMediaQuery";

import { wording } from "Wording";

import classes from "./style.module.scss";

import biola from "Images/biola.webp";
import bird from "Images/bird.webp";
import bird2 from "Images/bird2.webp";
import bird3 from "Images/bird3.webp";
import bird4 from "Images/bird4.webp";
import book from "Images/book.webp";
import children from "Images/children.webp";
import children2 from "Images/children2.webp";
import children3 from "Images/children3.webp";
import children4 from "Images/children4.webp";
import children5 from "Images/children5.webp";
import children6 from "Images/children6.webp";
import children7 from "Images/children7.webp";
import children8 from "Images/children8.webp";
import cloud from "Images/cloud.webp";
import cloud2 from "Images/cloud2.webp";
import decor1 from "Images/decor1.png";
import decor2 from "Images/decor2.png";
import decor3 from "Images/decor3.png";
import elephant from "Images/elephant.webp";
import flower from "Images/flower.webp";
import flower2 from "Images/flower2.webp";
import flower3 from "Images/flower3.webp";
import notes from "Images/notes.webp";
import sun from "Images/sun.webp";
import raport from "Images/raport.webp";
import tree from "Images/tree.webp";
import tree2 from "Images/tree2.webp";
import tree3 from "Images/tree3.webp";
import tree4 from "Images/tree4.webp";

import saxophone from "Images/saxophone.gif";
import treeGif from "Images/tree.gif";
import video from "Images/video.gif";

const Decoration = ({ type }) => {
  const mobileView = useMediaQuery("(max-width:992px)");
  const isBottom = type.includes("dbot");

  if (mobileView && isBottom) {
    return null;
  }
  if (mobileView) {
    return (
      <div className={classes.dmobile}>
        <img alt={wording.images} className={classes.decor1} src={decor1} />
        <img alt={wording.images} className={classes.cloud} src={cloud} />
        <img alt={wording.images} className={classes.cloud2} src={cloud} />
        <img alt={wording.images} className={classes.decor2} src={decor2} />
      </div>
    );
  }
  if (type === "dtop") {
    return (
      <div className={classes.dtop}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
        </div>
      </div>
    );
  }
  if (type === "dtop1") {
    return (
      <div className={classes.dtop1}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
        </div>
      </div>
    );
  }

  if (type === "dtop2") {
    return (
      <div className={classes.dtop2}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.bird} src={bird} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud2} />
        </div>
      </div>
    );
  }

  if (type === "dtop3") {
    return (
      <div className={classes.dtop3}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.bird} src={bird2} />
          <img alt={wording.images} className={classes.tree} src={tree} />
        </div>
      </div>
    );
  }

  if (type === "dtop4") {
    return (
      <div className={classes.dtop4}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.decor3} src={bird3} />
        </div>
      </div>
    );
  }

  if (type === "dtop5") {
    return (
      <div className={classes.dtop5}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.decor3} src={bird4} />
        </div>
      </div>
    );
  }

  if (type === "dtop6") {
    return (
      <div className={classes.dtop6}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.decor3} src={saxophone} />
        </div>
      </div>
    );
  }

  if (type === "dtop7") {
    return (
      <div className={classes.dtop7}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.decor3} src={sun} />
        </div>
      </div>
    );
  }

  if (type === "dtop8") {
    return (
      <div className={classes.dtop8}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.decor3} src={notes} />
        </div>
      </div>
    );
  }

  if (type === "dtop9") {
    return (
      <div className={classes.dtop9}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={decor1} />
          <img alt={wording.images} className={classes.cloud} src={cloud} />
          <img alt={wording.images} className={classes.cloud2} src={cloud} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.cloud3} src={cloud} />
          <img alt={wording.images} className={classes.decor3} src={raport} />
        </div>
      </div>
    );
  }

  if (type === "dbot1") {
    return (
      <div className={classes.dbot1}>
        <img alt={wording.images} className={classes.decor2} src={decor2} />
      </div>
    );
  }

  if (type === "dbot2") {
    return (
      <div className={classes.dbot2}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.children} src={children} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor2} src={decor2} />
          <img alt={wording.images} className={classes.tree} src={treeGif} />
        </div>
      </div>
    );
  }

  if (type === "dbot3") {
    return (
      <div className={classes.dbot3}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.biola} src={biola} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor2} src={decor2} />
        </div>
      </div>
    );
  }
  if (type === "dbot4") {
    return (
      <div className={classes.dbot4}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={children6} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor2} src={decor2} />
          <img alt={wording.images} className={classes.decor3} src={children7} />
        </div>
      </div>
    );
  }

  if (type === "dbot5") {
    return (
      <div className={classes.dbot5}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={children2} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor2} src={decor2} />
        </div>
      </div>
    );
  }

  if (type === "dbot6") {
    return (
      <div className={classes.dbot6}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={flower} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor3} src={book} />
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }

  if (type === "dbot7") {
    return (
      <div className={classes.dbot7}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={video} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor3} src={children3} />
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }

  if (type === "dbot8") {
    return (
      <div className={classes.dbot8}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={tree2} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor3} src={children4} />
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }

  if (type === "dbot9") {
    return (
      <div className={classes.dbot9}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={tree3} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }
  if (type === "dbot10") {
    return (
      <div className={classes.dbot10}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={elephant} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor3} src={tree4} />
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }
  if (type === "dbot11") {
    return (
      <div className={classes.dbot11}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={children5} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor3} src={flower2} />
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }

  if (type === "dbot12") {
    return (
      <div className={classes.dbot12}>
        <div className={classes.left}>
          <img alt={wording.images} className={classes.decor1} src={flower3} />
        </div>
        <div className={classes.right}>
          <img alt={wording.images} className={classes.decor3} src={children8} />
          <img alt={wording.images} className={classes.decor2} src={decor3} />
        </div>
      </div>
    );
  }
  return null;
};

Decoration.propTypes = {
  type: PropTypes.string,
};

export default Decoration;
