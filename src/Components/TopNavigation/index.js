import React from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { wording } from "Wording";

import classes from "./style.module.scss";

import backIcon from "Images/backIcon.webp";
import materi from "Images/materi.webp";
import game from "Images/game.webp";

const TopNavigation = ({ title, isGame, isMateri, backToDashboard, noBackButton }) => {
  const navigate = useNavigate();

  const go = () => {
    if (isGame) {
      navigate("/game");
    }
    if (isMateri) {
      navigate("/materi");
    }
  };

  const onBack = () => {
    if (backToDashboard) {
      navigate("/dashboard");
    } else {
      navigate(-1);
    }
  };

  return (
    <div className={classes.topNavigation}>
      <div className={classes.back} onClick={onBack}>
        {!noBackButton && <img className={classes.backIcon} alt={wording.images} src={backIcon} />}
      </div>
      <div className={classes.title}>{title}</div>
      <div className={classes.next} onClick={go}>
        {(isGame || isMateri) && <img className={classes.nextIcon} alt={wording.images} src={isGame ? game : materi} />}
      </div>
    </div>
  );
};

TopNavigation.propTypes = {
  title: PropTypes.string,
  isGame: PropTypes.bool,
  isMateri: PropTypes.bool,
  backToDashboard: PropTypes.bool,
  noBackButton: PropTypes.bool,
};

export default TopNavigation;
