import React, { useEffect, useState, useRef } from "react";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";
import sumBy from "lodash/sumBy";
import moment from "moment/moment";
import Collapse from "@mui/material/Collapse";
import classNames from "classnames";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ReactAudioPlayer from "react-audio-player";
import { setMuteBacksound } from "Containers/App/actions";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

import { Carousel } from "react-responsive-carousel";

import { wording } from "Wording";

import PopupGame from "Components/PopupGame";
import PopupPrompt from "Components/PopupPrompt";

import classes from "./style.module.scss";

import play from "Images/play.webp";
import pause from "Images/pause.webp";

import sound1 from "Sound/1.ogg";
import sound2 from "Sound/2.ogg";
import sound3 from "Sound/3.ogg";
import sound4 from "Sound/4.ogg";
import sound5 from "Sound/5.ogg";
import sound6 from "Sound/6.ogg";
import sound7 from "Sound/7.ogg";
import sound8 from "Sound/8.ogg";
import sound9 from "Sound/9.ogg";
import sound10 from "Sound/10.ogg";

const GameBoardSound = ({
  levelConfig,
  levelData,
  quest,
  setLevelConfig,
  resetLevelConfig,
  setLevelData,
  title,
  level,
  nextUnlockLevel,
  isRenderSound,
  openAnswerKey,
  onSendData,
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const tab = [...Array(10).keys()];
  const [popup, setPopup] = useState({ open: false, title: "" });
  const [openTab, setOpenTab] = useState(true);
  const [isCompleted, setIsCompleted] = useState(false);
  const [finishPrompt, setFinishPropmt] = useState(false);
  const [openSummary, setOpenSummary] = useState(true);
  const [statusAudio, setStatusAudio] = useState([]);
  const { currentIndex, answer } = levelConfig;
  const currentAnswer = answer[currentIndex];
  const currentQuest = quest[currentIndex];
  const lockAnswer = currentAnswer.lockAnswer;
  const allowBack = currentIndex > 0;
  const allowNext = currentIndex < quest.length - 1;
  const isFinish = levelConfig.isCompleted;

  const midiData = [sound1, sound2, sound3, sound4, sound5, sound6, sound7, sound8, sound9, sound10];

  const arrRef = [...Array(10).keys()];
  const statusRef = useRef([]);
  const soundRef = useRef([]);

  useEffect(() => {
    const summary = levelConfig.answer.filter((item) => {
      if (!isEmpty(item.answer)) {
        return item;
      }
      return null;
    });
    if (summary.length === quest.length) {
      setIsCompleted(true);
    } else {
      setIsCompleted(false);
    }
  }, [levelConfig, quest]);

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  useEffect(() => {
    if (isRenderSound) {
      setStatusAudio(statusRef.current);
    }
  }, [statusRef, isRenderSound]);

  const onSelect = (value) => {
    const data = answer?.map((item) => {
      if (item.no === currentIndex + 1) {
        return { ...item, answer: value };
      }
      return item;
    });
    dispatch(
      setLevelConfig({
        ...levelConfig,
        answer: data,
      })
    );
  };

  const onBack = () => {
    if (allowBack) {
      dispatch(
        setLevelConfig({
          ...levelConfig,
          currentIndex: currentIndex - 1,
        })
      );
    }
  };

  const onNext = () => {
    if (allowNext) {
      dispatch(
        setLevelConfig({
          ...levelConfig,
          currentIndex: currentIndex + 1,
        })
      );
    }
  };

  const onNextLevel = () => {
    navigate("/game");
  };

  const onClickTab = (item) => {
    dispatch(
      setLevelConfig({
        ...levelConfig,
        currentIndex: item,
        openBoard: true,
      })
    );
  };

  const onResetLevelConfig = () => {
    dispatch(resetLevelConfig());
  };

  const closeFinishPrompt = () => {
    setFinishPropmt(false);
  };

  const onFinish = () => {
    const checkedAnswer = answer.map((item, index) => {
      if (!item.result) {
        let result = "incorrect";
        let score = 0;
        if (item.answer === quest[index].correctAnswer) {
          result = "correct";
          score = 10;
        }
        return { ...item, lockAnswer: true, result, score };
      }
      return item;
    });
    const timeStamp = moment(new Date()).format("LL, LT");
    const newTotalScore = sumBy(checkedAnswer, "score");
    const historyScore = [...levelData?.historyScore, { score: newTotalScore, timeStamp }];
    if (isEmpty(levelData?.firstAnswer)) {
      dispatch(
        setLevelData({
          firstAnswer: checkedAnswer,
          currentScore: newTotalScore,
          timeStamp,
          historyScore,
        })
      );
      onSendData(newTotalScore, timeStamp);
    } else {
      dispatch(
        setLevelData({
          ...levelData,
          historyScore,
        })
      );
    }
    dispatch(
      setLevelConfig({
        ...levelConfig,
        answer: checkedAnswer,
        totalScore: newTotalScore,
        isCompleted: true,
        openBoard: false,
      })
    );

    if (nextUnlockLevel) {
      nextUnlockLevel();
    }

    setFinishPropmt(false);
    setOpenSummary(true);
  };

  const onCheckAnswer = () => {
    const isCorrect = currentAnswer.answer === currentQuest.correctAnswer;
    const data = answer?.map((item) => {
      if (item.no === currentIndex + 1) {
        let result = "incorrect";
        let score = 0;
        if (isCorrect) {
          result = "correct";
          score = 10;
        }
        return { ...item, lockAnswer: true, result, score };
      }
      return item;
    });
    dispatch(
      setLevelConfig({
        ...levelConfig,
        answer: data,
      })
    );

    if (isCorrect) {
      setPopup({ open: true, title: wording.correctAnswer });
    } else {
      setPopup({ open: true, title: wording.incorrectAnswer });
    }
  };

  const onPlay = (id) => {
    arrRef.forEach((item) => {
      if (item === id) {
        soundRef.current[item].audioEl.current.play();
      } else {
        soundRef.current[item].audioEl.current.pause();
        soundRef.current[item].audioEl.current.currentTime = 0;
      }
    });

    const newStatus = statusAudio.map((item, index) => {
      if (index === id) {
        item = "play";
      } else {
        item = "stop";
      }
      return item;
    });
    setStatusAudio(newStatus);
  };

  const onPause = (id) => {
    soundRef.current[id].audioEl.current.pause();
    const newStatus = statusAudio.map(() => {
      return "stop";
    });
    setStatusAudio(newStatus);
  };

  const onEnded = () => {
    const newStatus = statusAudio.map(() => {
      return "stop";
    });
    setStatusAudio(newStatus);
  };

  const genBtnAudio = (status, id) => {
    if (!isEmpty(status)) {
      if (status === "stop") {
        return (
          <div onClick={() => onPlay(id)} className={classes.btnPlay}>
            <img alt={wording.images} src={play} className={classes.btn} />
          </div>
        );
      }

      if (status === "play") {
        return (
          <div onClick={() => onPause(id)} className={classes.btnPlay}>
            <img alt={wording.images} src={pause} className={classes.btn} />
          </div>
        );
      }
    }

    return <div className={classes.loading}>{wording.loadSound}</div>;
  };

  const renderMidi = () => {
    return midiData.map((item, index) => {
      return (
        <ReactAudioPlayer
          key={index}
          src={item}
          ref={(el) => {
            soundRef.current[index] = el;
          }}
          onLoadedMetadata={() => {
            statusRef.current[index] = "stop";
          }}
          onEnded={onEnded}
        />
      );
    });
  };

  const renderOption = (option) => {
    if (lockAnswer) {
      return option.map((soal, key) => {
        const isSelected = currentAnswer.answer === soal.value;
        const correctOption = currentQuest.correctAnswer === soal.value;
        return (
          <div
            className={classNames({
              [classes.answerItem]: true,
              [classes.base]: true,
              [classes.locked]: true,
              [classes.correct]: correctOption,
              [classes.incorrect]: isSelected && !correctOption,
            })}
            key={key}
          >
            {soal.detail}
          </div>
        );
      });
    }
    return option.map((soal, key) => {
      const isSelected = currentAnswer.answer === soal.value;
      return (
        <div
          className={classNames({
            [classes.answerItem]: true,
            [classes.base]: !isSelected,
            [classes.selected]: isSelected,
          })}
          key={key}
          onClick={() => onSelect(soal.value)}
        >
          {soal.detail}
          {soal.detailImg && (
            <div className={classes.detailImg}>
              <img alt={wording.images} src={soal.detailImg} />
            </div>
          )}
        </div>
      );
    });
  };

  const renderTab = () => {
    return tab.map((item, index) => {
      const isSelected = currentIndex === item && levelConfig?.openBoard;
      const isChoosen = !isEmpty(answer[item]?.answer);
      const isCorrect = answer[item]?.result === "correct";
      const isIncorrect = answer[item]?.result === "incorrect";
      return (
        <div
          key={index}
          className={classNames({
            [classes.tab]: true,
            [classes.base]: true,
            [classes.selected]: isSelected,
            [classes.choosen]: isChoosen && !isCorrect && !isIncorrect,
            [classes.correct]: isCorrect,
            [classes.incorrect]: isIncorrect,
          })}
          onClick={() => onClickTab(item)}
        >
          {item + 1}
        </div>
      );
    });
  };

  return (
    <div className={classes.content}>
      {isRenderSound && <div className={classes.midi}>{renderMidi()}</div>}
      {isFinish && (
        <div className={classes.summary}>
          <div
            className={classes.summaryTab}
            onClick={() => setOpenSummary(!openSummary)}
            style={{ cursor: "pointer" }}
          >
            <div className={classes.text}>{wording.summary}</div>
            <ExpandMoreIcon />
          </div>

          <Collapse in={openSummary}>
            <div className={classes.result}>
              <div className={classes.resultTitle}>{wording.scoreText}</div>
              <table className={classes.table}>
                <tbody>
                  <tr>
                    <td>{wording.difficult}</td>
                    <td>:</td>
                    <td>{level}</td>
                  </tr>
                  <tr>
                    <td>{wording.topic}</td>
                    <td>:</td>
                    <td>{title.toLowerCase()}</td>
                  </tr>
                  <tr>
                    <td>{wording.score}</td>
                    <td>:</td>
                    <td>{levelConfig?.totalScore}</td>
                  </tr>
                </tbody>
              </table>
              <div className={classes.btnWrapper}>
                <button className={classes.btnKey} onClick={openAnswerKey}>
                  {wording.openAnswerKey}
                </button>
                <button className={classes.btnTryAgain} onClick={onResetLevelConfig}>
                  {wording.tryAgain}
                </button>
                <button className={classes.btnTryAgain} onClick={onNextLevel}>
                  {wording.nextLevel}
                </button>
              </div>
            </div>
          </Collapse>
        </div>
      )}
      <div className={classes.navigation}>
        <div className={classes.navTab} onClick={() => setOpenTab(!openTab)} style={{ cursor: "pointer" }}>
          <div className={classes.text}>{level}</div>
          <ExpandMoreIcon />
        </div>

        <Collapse in={openTab}>
          <div className={classes.navAction}>{renderTab()}</div>
        </Collapse>
      </div>

      <div className={classes.quizWrapper}>
        <Collapse in={levelConfig?.openBoard}>
          <Carousel
            swipeable={false}
            emulateTouch={false}
            showStatus={false}
            showIndicators={false}
            selectedItem={currentIndex}
            showArrows={false}
            showThumbs={false}
            onS
            onNext={onNext}
            onBack={onBack}
          >
            {quest.map((item, index) => {
              return (
                <div className={classes.quizBoard} key={index}>
                  <div className={classes.quest}>
                    <div className={classes.questTitle}>{item.title}</div>
                    <div className={classes.questDescription}>
                      <div className={classes.descText}>{item.description}</div>
                      {item.imgUrl && (
                        <div className={classes.descImg}>
                          <img alt={wording.images} src={item.imgUrl} className={classes.img} />
                        </div>
                      )}
                      {item.imgUrl2 && (
                        <div className={classes.descImg}>
                          <img alt={wording.images} src={item.imgUrl2} className={classes.img2} />
                        </div>
                      )}
                      {item?.descriptionSound && (
                        <div className={classes.audioWrapper}>
                          {item.descriptionSound.map((item, index) => {
                            return (
                              <div className={classes.audio} key={index}>
                                <div className={classes.audioPlayer}>
                                  <div className={classes.audioText}>{`${item.option})`}</div>
                                  {genBtnAudio(statusAudio[item.id], item.id)}
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      )}
                    </div>
                  </div>
                  <div className={classes.answer}>
                    <div className={classes.answerOption}>{renderOption(item.choiche)}</div>
                  </div>
                </div>
              );
            })}
          </Carousel>
          <div className={classes.navWrapper2}>
            {!currentAnswer.lockAnswer && (
              <button onClick={onCheckAnswer} className={classes.btnCheck} disabled={!currentAnswer.answer}>
                {wording.checkAnswer}
              </button>
            )}
            {isCompleted && !isFinish && (
              <button className={classes.btnFinish} onClick={() => setFinishPropmt(true)}>
                {wording.finish}
              </button>
            )}
          </div>
          <div className={classes.navWrapper}>
            <button className={classes.btnNav} onClick={onBack} disabled={!allowBack}>
              {wording.previousQuest}
            </button>
            <button className={classes.btnNav} onClick={onNext} disabled={!allowNext}>
              {wording.nextQuest}
            </button>
          </div>
        </Collapse>
      </div>
      <PopupGame
        open={popup.open}
        message={currentQuest.pembahasan}
        title={popup.title}
        isCorrect
        btnPrimaryText={wording.close}
        onClose={() => setPopup({ ...popup, open: false })}
        onClickBtnPrimary={() => setPopup({ ...popup, open: false })}
      />
      <PopupPrompt
        open={finishPrompt}
        onClose={closeFinishPrompt}
        title={wording.promptFinishText}
        onClickYes={onFinish}
        onClickNo={closeFinishPrompt}
      />
    </div>
  );
};

GameBoardSound.propTypes = {
  levelConfig: PropTypes.object,
  levelData: PropTypes.object,
  quest: PropTypes.array,
  setLevelConfig: PropTypes.func,
  resetLevelConfig: PropTypes.func,
  setLevelData: PropTypes.func,
  nextUnlockLevel: PropTypes.func,
  title: PropTypes.string,
  level: PropTypes.string,
  answerKey: PropTypes.string,
  isRenderSound: PropTypes.bool,
  openAnswerKey: PropTypes.func,
  onSendData: PropTypes.func,
};

export default GameBoardSound;
