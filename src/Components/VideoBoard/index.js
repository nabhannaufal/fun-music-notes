import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import ReactPlayer from "react-player";
import { useDispatch } from "react-redux";
import { setMuteBacksound } from "Containers/App/actions";
import useMediaQuery from "@mui/material/useMediaQuery";

import classes from "./style.module.scss";
import { wording } from "Wording";

const VideoBoard = ({ url }) => {
  const dispatch = useDispatch();
  const [openVideo, setOpenVideo] = useState(false);
  const mobileView = useMediaQuery("(max-width:992px)");
  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  let style = {
    width: "796px",
    height: "448px",
  };

  if (mobileView) {
    style = {
      width: "398px",
      height: "224px",
    };
  }
  return (
    <div className={classes.menuWrapper}>
      {!openVideo && <div className={classes.loading}>{wording.loadVideo}</div>}
      <ReactPlayer url={url} width={style.width} height={style.height} controls onReady={() => setOpenVideo(true)} />
    </div>
  );
};

VideoBoard.propTypes = {
  url: PropTypes.string,
};

export default VideoBoard;
