import React from "react";
import PropTypes from "prop-types";
import { Dialog } from "@mui/material";

import { wording } from "Wording";

import classes from "./style.module.scss";

const PopupDialog = ({ open, onClose, title, message }) => {
  const renderPopup = (element) => {
    let props = {
      open,
      onClose,
    };
    return (
      <Dialog {...props} PaperProps={{ style: { borderRadius: "16px" } }}>
        {element}
      </Dialog>
    );
  };

  const popupWrapper = (
    <div className={classes.popupWrapper}>
      <div className={classes.title}>{title}</div>
      <div className={classes.message}>{message}</div>
      <button type="button" className={classes.button} onClick={onClose}>
        {wording.close}
      </button>
    </div>
  );
  return renderPopup(popupWrapper);
};

PopupDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
};

export default PopupDialog;
