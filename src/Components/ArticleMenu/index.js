import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import ReactAudioPlayer from "react-audio-player";
import isEmpty from "lodash/isEmpty";
import { useDispatch } from "react-redux";
import { Carousel } from "react-responsive-carousel";
import { setMuteBacksound } from "Containers/App/actions";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { wording } from "Wording";

import back from "Images/back.png";
import next from "Images/next.png";
import play from "Images/play.webp";
import pause from "Images/pause.webp";

import materi3 from "Sound/materi3&5.ogg";
import materi4 from "Sound/materi4.ogg";
import materi5 from "Sound/materi5.ogg";
import materi6 from "Sound/materi6.ogg";
import materi13a from "Sound/materi13a.ogg";
import materi13b from "Sound/materi13b.ogg";
import materi39 from "Sound/materi39.ogg";

import classes from "./style.module.scss";

const ArticleMenu = ({ materi }) => {
  const arrRef = [...Array(7).keys()];
  const audioData = [materi3, materi4, materi5, materi6, materi13a, materi13b, materi39];
  const [index, setIndex] = useState(0);
  const [statusAudio, setStatusAudio] = useState(["false", "false", "false", "false", "false", "false", "false"]);
  const dispatch = useDispatch();

  const soundRef = useRef([]);
  const statusRef = useRef([]);

  useEffect(() => {
    dispatch(setMuteBacksound(true));
  }, [dispatch]);

  useEffect(() => {
    setStatusAudio(statusRef.current);
  }, [statusRef]);

  const onPlay = (id) => {
    arrRef.forEach((item) => {
      if (item === id) {
        soundRef.current[item].audioEl.current.play();
      } else {
        soundRef.current[item].audioEl.current.pause();
        soundRef.current[item].audioEl.current.currentTime = 0;
      }
    });

    const newStatus = statusAudio.map((item, index) => {
      if (index === id) {
        item = "play";
      } else {
        item = "stop";
      }
      return item;
    });
    setStatusAudio(newStatus);
  };

  const onPause = (id) => {
    soundRef.current[id].audioEl.current.pause();
    soundRef.current[id].audioEl.current.currentTime = 0;
    const newStatus = statusAudio.map(() => {
      return "stop";
    });
    setStatusAudio(newStatus);
  };

  const onEnded = () => {
    const newStatus = statusAudio.map(() => {
      return "stop";
    });
    setStatusAudio(newStatus);
  };

  const genStyleButton = (id) => {
    let style;
    switch (id) {
      case "default1":
        style = classes.default1;
        break;
      case "default2":
        style = classes.default2;
        break;
      case "default3":
        style = classes.default3;
        break;
      case "default4":
        style = classes.default4;
        break;
      default:
        style = classes.default;
    }
    return style;
  };

  const genBtnAudio = (id, idStyle) => {
    const status = statusAudio[id];
    const style = genStyleButton(idStyle);
    if (!isEmpty(status)) {
      if (status === "stop") {
        return (
          <div className={`${classes.btnPlay} ${style}`}>
            <div onClick={() => onPlay(id)} className={classes.btn}>
              <LazyLoadImage effect="blur" alt={wording.images} src={play} />
            </div>
          </div>
        );
      }

      if (status === "play") {
        return (
          <div className={`${classes.btnPlay} ${style}`}>
            <div onClick={() => onPause(id)} className={classes.btn}>
              <LazyLoadImage effect="blur" alt={wording.images} src={pause} />
            </div>
          </div>
        );
      }
    }

    return <div className={classes.loading}>{wording.loadSound}</div>;
  };

  const onNext = () => {
    if (index === materi.length - 1) {
      setIndex(0);
    } else {
      setIndex(index + 1);
    }
  };

  const onBack = () => {
    if (index === 0) {
      setIndex(materi.length - 1);
    } else {
      setIndex(index - 1);
    }
  };

  const genAudio = () => {
    return (
      <div className={classes.audioPlayer}>
        {audioData.map((item, index) => {
          return (
            <ReactAudioPlayer
              key={index}
              src={item}
              ref={(element) => {
                soundRef.current[index] = element;
              }}
              onLoadedMetadata={() => {
                statusRef.current[index] = "stop";
              }}
              onEnded={onEnded}
            />
          );
        })}
      </div>
    );
  };

  return (
    <div className={classes.menuWrapper}>
      {genAudio()}
      <div className={classes.content}>
        <div className={classes.arrow} onClick={onBack}>
          <LazyLoadImage effect="blur" alt={wording.images} src={back} className={classes.icon} />
        </div>
        <div className={classes.carousel}>
          <Carousel
            showArrows={false}
            showThumbs={false}
            showIndicators={false}
            selectedItem={index}
            swipeable
            sinfiniteLoop
          >
            {materi.map((item, index) => {
              return (
                <div key={index} className={classes.materi}>
                  <LazyLoadImage effect="blur" alt={wording.images} src={item.img} className={classes.materiImg} />
                  {item.sound &&
                    item.sound.map((value, idx) => {
                      return <div key={idx}>{genBtnAudio(value.id, value.style)}</div>;
                    })}
                </div>
              );
            })}
          </Carousel>
        </div>
        <div className={classes.arrow} onClick={onNext}>
          <LazyLoadImage effect="blur" alt={wording.images} src={next} className={classes.icon} />
        </div>
      </div>
    </div>
  );
};

ArticleMenu.propTypes = {
  materi: PropTypes.array,
};

export default ArticleMenu;
