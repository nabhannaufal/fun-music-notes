import React, { useRef, useEffect, useState } from "react";
import PropTypes from "prop-types";
import backsound from "Sound/backsound.mp3";
import ReactAudioPlayer from "react-audio-player";

const SoundControl = ({ isPlay }) => {
  const [volume, setVolume] = useState(0);
  const play = useRef(null);

  useEffect(() => {
    if (isPlay) {
      play.current.audioEl.current.play();
      const fadeIn = () => {
        if (volume < 0.9) {
          const newVolume = volume + 0.1;
          setVolume(Number(newVolume.toFixed(1)));
        }
      };
      setTimeout(fadeIn, 200);
    } else {
      const fadeOut = () => {
        if (volume > 0) {
          const newVolume = volume - 0.1;
          setVolume(Number(newVolume.toFixed(1)));
        }
        if (volume <= 0) {
          play.current.audioEl.current.pause();
        }
      };
      setTimeout(fadeOut, 50);
    }
  }, [isPlay, volume]);

  return <ReactAudioPlayer src={backsound} ref={play} volume={volume} loop />;
};

SoundControl.propTypes = {
  isPlay: PropTypes.bool,
};

export default SoundControl;
