import React from "react";
import PropTypes from "prop-types";
import { Dialog, Drawer, useMediaQuery } from "@mui/material";
import { wording } from "Wording";
import closeIcon from "Images/closeIcon.png";

import classes from "./style.module.scss";

const PopupGame = ({
  open,
  onClose,
  title,
  message,
  btnPrimaryText,
  onClickBtnPrimary,
  btnSecondaryText,
  onClickBtnSecondary,
}) => {
  const mobileView = useMediaQuery("(max-width:992px)");
  const renderPopup = (element) => {
    let props = {
      open,
      onClose,
    };

    if (mobileView) {
      props = {
        anchor: "bottom",
        PaperProps: { style: { borderRadius: "16px 16px 0 0" } },
        ...props,
      };
      return <Drawer {...props}>{element}</Drawer>;
    }
    return (
      <Dialog {...props} PaperProps={{ style: { borderRadius: "16px" } }}>
        {element}
      </Dialog>
    );
  };

  const popupWrapper = (
    <div className={classes.popupWrapper}>
      <div className={classes.popupTab}>
        <div className={classes.closeIcon} onClick={onClose}>
          <img src={closeIcon} alt={wording.images} className={classes.icon} />
        </div>
      </div>
      <div className={classes.title}>{title}</div>
      <div className={classes.message} dangerouslySetInnerHTML={{ __html: message }} />
      <div className={classes.btnWrapper}>
        {btnPrimaryText && (
          <button type="button" className={classes.button} onClick={() => onClickBtnPrimary()}>
            {btnPrimaryText}
          </button>
        )}
        {btnSecondaryText && (
          <button type="button" className={classes.button2} onClick={() => onClickBtnSecondary()}>
            {btnSecondaryText}
          </button>
        )}
      </div>
    </div>
  );
  return renderPopup(popupWrapper);
};

PopupGame.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  message: PropTypes.string,
  btnPrimaryText: PropTypes.string,
  btnSecondaryText: PropTypes.string,
  onClickBtnPrimary: PropTypes.func,
  onClickBtnSecondary: PropTypes.func,
};

export default PopupGame;
