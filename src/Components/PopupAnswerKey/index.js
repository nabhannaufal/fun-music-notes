import React from "react";
import PropTypes from "prop-types";
import { Dialog, Drawer, useMediaQuery } from "@mui/material";
import { wording } from "Wording";

import answerKey from "Utils/answerKey";
import closeIcon from "Images/closeIcon.png";

import classes from "./style.module.scss";

const PopupAnswerKey = ({ open, onClose, title, type }) => {
  const mobileView = useMediaQuery("(max-width:992px)");
  const renderPopup = (element) => {
    let props = {
      open,
      onClose,
    };

    if (mobileView) {
      props = {
        anchor: "bottom",
        PaperProps: { style: { borderRadius: "16px 16px 0 0" } },
        ...props,
      };
      return <Drawer {...props}>{element}</Drawer>;
    }
    return (
      <Dialog {...props} PaperProps={{ style: { borderRadius: "16px", maxWidth: "1200px" } }}>
        {element}
      </Dialog>
    );
  };

  const popupWrapper = (
    <div className={classes.popupWrapper}>
      <div className={classes.popupTab}>
        <div className={classes.closeIcon} onClick={onClose}>
          <img src={closeIcon} alt={wording.images} className={classes.icon} />
        </div>
      </div>
      <div className={classes.title}>{title}</div>
      <div className={classes.content}>{answerKey(type)}</div>
    </div>
  );
  return renderPopup(popupWrapper);
};

PopupAnswerKey.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  title: PropTypes.string,
  btnPrimaryText: PropTypes.string,
  btnSecondaryText: PropTypes.string,
};

export default PopupAnswerKey;
