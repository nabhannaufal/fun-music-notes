import React from "react";
import PropTypes from "prop-types";
import { Dialog, Drawer, useMediaQuery } from "@mui/material";

import classes from "./style.module.scss";
import { wording } from "Wording";

const PopupHint = ({ open, onClose }) => {
  const mobileView = useMediaQuery("(max-width:992px)");
  const renderPopup = (element) => {
    let props = {
      open,
      onClose,
    };

    if (mobileView) {
      props = {
        anchor: "bottom",
        PaperProps: { style: { borderRadius: "16px 16px 0 0" } },
        ...props,
      };
      return <Drawer {...props}>{element}</Drawer>;
    }
    return (
      <Dialog {...props} PaperProps={{ style: { borderRadius: "16px" } }}>
        {element}
      </Dialog>
    );
  };

  const popupWrapper = (
    <div className={classes.popupWrapper}>
      <div className={classes.title}>{wording.hintGameTitle}</div>
      <div className={classes.message} dangerouslySetInnerHTML={{ __html: wording.hintGame }} />
      <div className={classes.btnWrapper}>
        <button type="button" className={classes.button} onClick={onClose}>
          {wording.close}
        </button>
      </div>
    </div>
  );
  return renderPopup(popupWrapper);
};

PopupHint.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onClickBtn: PropTypes.func,
};

export default PopupHint;
